package FileManager;

import EPortfolioModel.BannerComp;
import EPortfolioModel.ColorComp;
import EPortfolioModel.Component;
import EPortfolioModel.FooterComp;
import EPortfolioModel.HeaderComp;
import EPortfolioModel.ImageComp;
import EPortfolioModel.LayoutComp;
import EPortfolioModel.ListComp;
import EPortfolioModel.PageModel;
import EPortfolioModel.ParagraphComp;
import EPortfolioModel.SlideshowComp;
import EPortfolioModel.VideoComp;
import EPortfolioView.EPortfolioView;
import static EPortfolioView.StartupConstants.CSS_PAGE_WORKSPACE;
import static EPortfolioView.StartupConstants.PATH_EPFS;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;

/**
 *
 * @author KhaledAshraf
 */
public class EPortfolioFileManager {
    public static String JSON_TITLE = "title";
    public static String JSON_SLIDES = "slides";
    public static String JSON_PAGES = "pages";
    public static String JSON_TYPE = "type";
    public static String JSON_COMP = "components";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_VIDEO_PATH = "video_path";
    public static String JSON_VIDEO_FILE_NAME = "video_file_name";
    public static String JSON_BIMAGE_PATH = "banner_image_path";
    public static String JSON_PAGE_TITLE = "title";
    public static String JSON_PAGE_NUM = "page_number";
    public static String JSON_PAGE_LAYOUT = "layout";
    public static String JSON_HEIGHT = "height";
    public static String JSON_WIDTH = "width";
    public static String JSON_POS = "position";
    public static String JSON_EXT = ".json";
    public static String JSON_CAPTION = "caption";
    public static String JSON_COLOR = "color";
    public static String JSON_HEADER = "header";
    public static String JSON_PAR = "paragraph";
    public static String JSON_LIST = "list_item";
    public static String JSON_LISTS = "list";
    public static String JSON_FOOTER = "footer";
    public static String JSON_FONT = "font";
    public static String JSON_FONT_SIZE = "font_size";
    public static String SLASH = "/";
    
    
    public void saveEPf(EPortfolioView ui, ObservableList<PageModel> pagesToSave) throws IOException {
        // BUILD THE FILE PATH
        String pagesTitle = "" + ui.getPrimaryStage().getTitle();
        String jsonFilePath = PATH_EPFS + SLASH + pagesTitle + JSON_EXT;
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        // BUILD THE PAGES ARRAY
        JsonArray pagesJsonArray = makePagesJsonArray(pagesToSave);
        
        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject courseJsonObject = Json.createObjectBuilder()
                .add(JSON_TITLE, ui.getPrimaryStage().getTitle())
                .add(JSON_PAGES, pagesJsonArray)
                .build();
        
        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(courseJsonObject);
    }
    
    private JsonArray makePagesJsonArray(List<PageModel> pages) {
        JsonArrayBuilder jspa = Json.createArrayBuilder();
        for (PageModel page : pages) {
            JsonObject jsa = makePageModelJsonArray(page.getComponents(),
                    page.getTitle(), page.getPageNumber(), page.getLayout(),
                    page.getColor());
            jspa.add(jsa);
        }
        JsonArray jpA = jspa.build();
        return jpA;
    }
    
    private JsonObject makePageModelJsonArray(ObservableList<Component> components, String title, int pageNumber, String layout, String color) {
        JsonArrayBuilder jsa = Json.createArrayBuilder();
        for (Component component : components) {
            if(!(component instanceof ColorComp || component instanceof LayoutComp)) {
                JsonObject jso = makeComponentJsonObject(component);
                jsa.add(jso);
            }
        }
        JsonObject jsPMo = Json.createObjectBuilder()
                .add(JSON_PAGE_TITLE, title)
                .add(JSON_PAGE_NUM, pageNumber)
                .add(JSON_PAGE_LAYOUT, layout)
                .add(JSON_COLOR, color)
                .add(JSON_COMP, jsa)
                .build();
        
        JsonObject jA = jsPMo;
        return jA;
    }
    
    private JsonObject makeComponentJsonObject(Component component) {
        if(component.getType().equals("Paragraph")) {
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_TYPE, ((ParagraphComp)component).getType())
                    .add(JSON_PAR, ((ParagraphComp)component).getText())
                    .add(JSON_FONT, ((ParagraphComp)component).getFont())
                    .add(JSON_FONT_SIZE, ((ParagraphComp)component).getFontSize())
                    .build();
            return jso;
        }
        else if(component.getType().equals("Header")) {
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_TYPE, ((HeaderComp)component).getType())
                    .add(JSON_HEADER, ((HeaderComp)component).getText())
                    .add(JSON_FONT, ((HeaderComp)component).getFont())
                    .add(JSON_FONT_SIZE, ((HeaderComp)component).getFontSize())
                    .build();
            return jso;
        }
        else if(component.getType().equals("Image")) {
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_TYPE, ((ImageComp)component).getType())
                    .add(JSON_IMAGE_PATH, ((ImageComp)component).getImgFilePath())
                    .add(JSON_IMAGE_FILE_NAME, ((ImageComp)component).getImgFileName())
                    .add(JSON_HEIGHT, ((ImageComp)component).getHeight())
                    .add(JSON_WIDTH, ((ImageComp)component).getWidth())
                    .add(JSON_CAPTION, ((ImageComp)component).getCaption())
                    .add(JSON_POS, ((ImageComp)component).getPosition())
                    .build();
            return jso;
        }
        else if(component.getType().equals("Video")) {
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_TYPE, ((VideoComp)component).getType())
                    .add(JSON_VIDEO_PATH, ((VideoComp)component).getVideoFilePath())
                    .add(JSON_VIDEO_FILE_NAME, ((VideoComp)component).getVideoFileName())
                    .add(JSON_HEIGHT, ((VideoComp)component).getHeight())
                    .add(JSON_WIDTH, ((VideoComp)component).getWidth())
                    .add(JSON_CAPTION, ((VideoComp)component).getCaption())
                    .add(JSON_POS, ((VideoComp)component).getPosition())
                    .build();
            return jso;
        }
        else if(component.getType().equals("BannerImg")) {
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_TYPE, ((BannerComp)component).getType())
                    .add(JSON_IMAGE_PATH, ((BannerComp)component).getImgFilePath())
                    .add(JSON_IMAGE_FILE_NAME, ((BannerComp)component).getFileName())
                    .build();
            return jso;
        }
        else if(component.getType().equals("Footer")) {
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_TYPE, ((FooterComp)component).getType())
                    .add(JSON_FOOTER, ((FooterComp)component).getText())
                    .add(JSON_FONT, ((FooterComp)component).getFont())
                    .add(JSON_FONT_SIZE, ((FooterComp)component).getFontSize())
                    .build();
            return jso;
        }
        else if(component.getType().equals("List")) {
            JsonArray jlA = makeListJsonArray(((ListComp)component).getListArray());
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_TYPE, ((ListComp)component).getType())
                    .add(JSON_LISTS, jlA)
                    .build();
            return jso;
        }
        else if(component.getType().equals("Slideshow")) {
            JsonArray jslA = makeSlideShowJsonArray(((SlideshowComp)component).getImages(), ((SlideshowComp)component).getCaptions());
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_TYPE, ((SlideshowComp)component).getType())
                    .add(JSON_SLIDES, jslA)
                    .build();
            return jso;
        }
        return null;
    }
    
    private JsonArray makeListJsonArray(ArrayList<String> listItems) {
        JsonArrayBuilder jsLa = Json.createArrayBuilder();
        for (String listItem : listItems) {
            JsonObject jso = makeListItemJsonObject(listItem);
            jsLa.add(jso);
        }
        JsonArray jlA = jsLa.build();
        return jlA;
    }
    
    private JsonArray makeSlideShowJsonArray(ArrayList<String> images, ArrayList<String> captions) {
        JsonArrayBuilder jsla = Json.createArrayBuilder();
        int i = 0;
        for(String image : images) {
            String caption = captions.get(i);
            JsonObject jslo = makeSlideShowJsonObject(image, caption);
            jsla.add(jslo);
            i++;
        }
        JsonArray jsLA = jsla.build();
        return jsLA;
    }
    
    private JsonObject makeListItemJsonObject(String listItem) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_LIST, listItem)
                .build();
        return jso;
    }
    
    private JsonObject makeSlideShowJsonObject(String image, String caption) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_IMAGE_PATH, image)
                .add(JSON_CAPTION, caption)
                .build();
        return jso;
    }
    
    public void loadPages(EPortfolioView ui, ObservableList<PageModel> pages, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);
        
        // NOW LOAD THE COURSE
        pages.clear();
        ui.getPageWorkspace().getTabs().clear();
        ui.getPrimaryStage().setTitle(json.getString(JSON_TITLE));
        JsonArray jsonPageModelsArray = json.getJsonArray(JSON_PAGES);
        for (int i = 0; i < jsonPageModelsArray.size(); i++) {
            JsonObject pageModelJso = jsonPageModelsArray.getJsonObject(i);
            JsonArray components = loadArrayFromJSONObject(pageModelJso, JSON_COMP);
            System.out.println(pageModelJso.getInt(JSON_PAGE_NUM));
            
            PageModel newPage = new PageModel(ui, pageModelJso.getInt(JSON_PAGE_NUM));
            newPage.fillSlots(pageModelJso.getString(JSON_PAGE_TITLE), pageModelJso.getString(JSON_PAGE_LAYOUT),
                    pageModelJso.getString(JSON_COLOR), components);
            ui.getPageWorkspace().getTabs().add(new Tab(newPage.getTitle()));
            VBox vbox = new VBox();
            vbox.getStyleClass().add(CSS_PAGE_WORKSPACE);
            ui.getPageWorkspace().getTabs().get(i).setContent(new FlowPane(vbox));
            pages.add(newPage);
            int x = ui.getPageWorkspace().getSelectionModel().getSelectedIndex();
            System.out.println(x);
            ui.loadEPortfolio(pages.get(i), x + i);
            
        }
        //ui.reloadEPortfolio(pages.get(1));
//        ui.reloadEPortfolio(pages.get(1));
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    } 
    
    private JsonArray loadArrayFromJSONObject(JsonObject json, String arrayName) throws IOException {
        JsonArray jsonArray = json.getJsonArray(arrayName);
        return jsonArray;
    }
}
