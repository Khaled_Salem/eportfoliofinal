/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package FileManager;

import EPortfolioModel.PageModel;
import EPortfolioView.EPortfolioView;
import static EPortfolioView.StartupConstants.PATH_EPFS;
import static EPortfolioView.StartupConstants.PATH_SLIDE_SHOWS;
import EPortfolioView.WebController;
import java.io.File;
import java.io.IOException;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author KhaledAshraf
 */
public class FileController {
    private boolean saved;
    
    private EPortfolioView ui;
    
    private EPortfolioFileManager ePfIO;
    
    public FileController(EPortfolioView initUI, EPortfolioFileManager initEPfIO) {
        saved = true;
        ui = initUI;
        ePfIO = initEPfIO;
    }
    
    public void markAsEdited() {
        saved = false;
    }
    
    public boolean handleSaveEPf() throws Exception {
        try {
            ObservableList<PageModel> pagesToSave = ui.getPages();
            ePfIO.saveEPf(ui, pagesToSave);
            
            createDir(ui.getPrimaryStage().getTitle());
            
            
            saved = true;
            return true;
        } catch(IOException ioe) {
            return false;
        }
    }
    
    public boolean handleLoadEPf() {
        promptToOpen();
        ui.updateButtons();
        return true;
    }
    
    public void handleWebViewRequest() throws Exception {
        handleSaveEPf();
        Stage webStage = new Stage();
        Scene webScene = new Scene(new WebController(ui.getPrimaryStage().getTitle()));
        webStage.setScene(webScene);
        webStage.show();
    }
    
    public void createDir(String path) throws Exception {
        String rootPath = "./data/sites";
        String basePath = "./data/locker";
        String slideShowPath = "./data/sites/" + path;
        
        //Create a SlideShow folder!
        new File(rootPath, path).mkdir();
        
        //Create a CSS folder!
        new File(basePath, "CSS").mkdir();
        new File(slideShowPath, "CSS").mkdir();
        
        //Create a JavaScript File!
        new File(basePath, "Js").mkdir();
        new File(slideShowPath, "Js").mkdir();
        
        //Create a Image folder!
        new File(slideShowPath, "Imgs").mkdir();
        
        //Create an Icon folder!
        new File(slideShowPath, "Icons").mkdir();
    }
    
    private void promptToOpen() {
// AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser ePfFileChooser = new FileChooser();
        ePfFileChooser.setInitialDirectory(new File(PATH_EPFS));
        File selectedFile = ePfFileChooser.showOpenDialog(ui.getPrimaryStage());
        
// ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                ObservableList<PageModel> pages = ui.getPages();
                ePfIO.loadPages(ui, pages, selectedFile.getAbsolutePath());
                saved = false;
            } catch (Exception e) {
                e.printStackTrace();
// @todo
            }
        }
    }
}
