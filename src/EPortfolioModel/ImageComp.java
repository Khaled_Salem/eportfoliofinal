/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioModel;

/**
 *
 * @author KhaledAshraf
 */
public class ImageComp extends Component {
    private String imgFileName;
    private String imgFilePath;
    private String caption;
    private int width;
    private int height;
    private String position;
    private boolean isSelected;
    
    public ImageComp() {
        imgFileName = "";
        type = "Image";
        isSelected = false;
        caption = "";
        width = 0;
        height = 0;
    }

    /**
     * @return the imgFileName
     */
    public String getImgFileName() {
        return imgFileName;
    }

    /**
     * @param imgFileName the imgFileName to set
     */
    public void setImgFileName(String imgFileName) {
        this.imgFileName = imgFileName;
    }

    /**
     * @return the caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * @param caption the caption to set
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }
    
    public String getType() {
        return type;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }
    
    public boolean isIsSelected() {
        return isSelected;
    }

    /**
     * @param isSelected the isSelected to set
     */
    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * @return the imgFilePath
     */
    public String getImgFilePath() {
        return imgFilePath;
    }

    /**
     * @param imgFilePath the imgFilePath to set
     */
    public void setImgFilePath(String imgFilePath) {
        this.imgFilePath = imgFilePath;
    }
}
