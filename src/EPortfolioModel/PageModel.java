/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package EPortfolioModel;

import EPortfolioView.EPortfolioView;
import FileManager.EPortfolioFileManager;
import static FileManager.EPortfolioFileManager.JSON_BIMAGE_PATH;
import static FileManager.EPortfolioFileManager.JSON_CAPTION;
import static FileManager.EPortfolioFileManager.JSON_FONT;
import static FileManager.EPortfolioFileManager.JSON_FONT_SIZE;
import static FileManager.EPortfolioFileManager.JSON_FOOTER;
import static FileManager.EPortfolioFileManager.JSON_HEADER;
import static FileManager.EPortfolioFileManager.JSON_IMAGE_FILE_NAME;
import static FileManager.EPortfolioFileManager.JSON_IMAGE_PATH;
import static FileManager.EPortfolioFileManager.JSON_LIST;
import static FileManager.EPortfolioFileManager.JSON_LISTS;
import static FileManager.EPortfolioFileManager.JSON_PAR;
import static FileManager.EPortfolioFileManager.JSON_SLIDES;
import static FileManager.EPortfolioFileManager.JSON_TYPE;
import static FileManager.EPortfolioFileManager.JSON_VIDEO_FILE_NAME;
import static FileManager.EPortfolioFileManager.JSON_VIDEO_PATH;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author KhaledAshraf
 */
public class PageModel {
    private EPortfolioView ui;
    private String title;
    private String color;
    private String layout;
    private ObservableList<Component> components;
    private Component selectedComponent;
    private int pageNumber;
    
    public PageModel(EPortfolioView initUI, int pageNumber) {
        ui = initUI;
        title = "";
        color = "";
        layout = "";
        this.pageNumber = pageNumber;
        components = FXCollections.observableArrayList();
        reset();
    }
    
    public void reset() {
        getComponents().clear();
        setSelectedComponent(null);
    }
    
    public void fillSlots(String title, String layout, String color, JsonArray components) throws IOException {
        this.title = title;
        this.layout = layout;
        this.color = color;
        
        for(int i = 0; i < components.size(); i++) {
            JsonObject component = components.getJsonObject(i);
            String type = component.getString(JSON_TYPE);
            if(type.equals("Header"))
                createHeader(component);
            else if(type.equals("Footer"))
                createFooter(component);
            else if(type.equals("Paragraph"))
                createParagraph(component);
            else if(type.equals("BannerImg"))
                createBannerImg(component);
            else if(type.equals("Image"))
                createImage(component);
            else if(type.equals("Video"))
                createVideo(component);
            else if(type.equals("List")) {
                JsonArray listArray = loadArrayFromJSONObject(component, JSON_LISTS);
                createList(listArray);
            }
            else if(type.equals("Slideshow")) {
                JsonArray slideShowArray = loadArrayFromJSONObject(component, JSON_SLIDES);
                createSlideshow(slideShowArray);
            }
        }
    }
    
    private void createHeader(JsonObject component) {
        HeaderComp header = new HeaderComp();
        header.setText(component.getString(JSON_HEADER));
        header.setFont(component.getString(JSON_FONT));
        header.setFontSize(component.getString(JSON_FONT_SIZE));
        components.add(header);
    }
    
    private void createFooter(JsonObject component) {
        FooterComp footer = new FooterComp();
        footer.setText(component.getString(JSON_FOOTER));
        footer.setFont(component.getString(JSON_FONT));
        footer.setFontSize(component.getString(JSON_FONT_SIZE));
        components.add(footer);
    }
    
    private void createParagraph(JsonObject component) {
        ParagraphComp paragraph = new ParagraphComp();
        paragraph.setText(component.getString(JSON_PAR));
        paragraph.setFont(component.getString(JSON_FONT));
        paragraph.setFontSize(component.getString(JSON_FONT_SIZE));
        components.add(paragraph);
    }
    
    private void createBannerImg(JsonObject component) {
        BannerComp bannerImg = new BannerComp();
        bannerImg.setFileName(component.getString(JSON_IMAGE_FILE_NAME));
        bannerImg.setImgFilePath(component.getString(JSON_BIMAGE_PATH));
        components.add(bannerImg);
    }
    
    private void createImage(JsonObject component) {
        ImageComp image = new ImageComp();
        image.setImgFileName(component.getString(JSON_IMAGE_FILE_NAME));
        image.setImgFilePath(component.getString(JSON_IMAGE_PATH));
        image.setCaption(component.getString(JSON_CAPTION));
        components.add(image);
    }
    
    private void createVideo(JsonObject component) {
        VideoComp video = new VideoComp();
        video.setVideoFileName(component.getString(JSON_VIDEO_FILE_NAME));
        video.setVideoFilePath(component.getString(JSON_VIDEO_PATH));
        video.setCaption(component.getString(JSON_CAPTION));
        components.add(video);
    }
    
    private void createList(JsonArray listArray) {
        ListComp listComp = new ListComp();
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < listArray.size(); i++) {
            JsonObject listItem = listArray.getJsonObject(i);
            list.add(listItem.getString(JSON_LIST));
        }
        listComp.setListArray(list);
        components.add(listComp);
    }
    
    private void createSlideshow(JsonArray listArray) {
        SlideshowComp slideShowComp = new SlideshowComp();
        ArrayList<String> arrayImage = new ArrayList<String>();
        ArrayList<String> arrayCaption = new ArrayList<String>();
        for(int i = 0; i < listArray.size(); i++) {
            JsonObject slide = listArray.getJsonObject(i);
            arrayImage.add(slide.getString(JSON_IMAGE_PATH));
            arrayCaption.add(slide.getString(JSON_CAPTION));
        }
        slideShowComp.setImages(arrayImage);
        slideShowComp.setCaptions(arrayCaption);
        components.add(slideShowComp);
    }
    
    
    private JsonArray loadArrayFromJSONObject(JsonObject json, String arrayName) throws IOException {
        JsonArray jsonArray = json.getJsonArray(arrayName);
        return jsonArray;
    }
    
    public boolean isComponentSelected() {
        return getSelectedComponent() != null;
    }
    
    /**
     * @return the selectedComponent
     */
    public Component getSelectedComponent() {
        return selectedComponent;
    }
    
    /**
     * @param selectedComponent the selectedComponent to set
     */
    public void setSelectedComponent(Component selectedComponent) {
        this.selectedComponent = selectedComponent;
    }
    
    /**
     * @return the components
     */
    public ObservableList<Component> getComponents() {
        return components;
    }
    
    /**
     * @param components the components to set
     */
    public void setComponents(ObservableList<Component> components) {
        this.components = components;
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
     * @return the ui
     */
    public EPortfolioView getUi() {
        return ui;
    }
    
    /**
     * @return the pageNumber
     */
    public int getPageNumber() {
        return pageNumber;
    }
    
    /**
     * @param pageNumber the pageNumber to set
     */
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
    
    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }
    
    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }
    
    /**
     * @return the layout
     */
    public String getLayout() {
        return layout;
    }
    
    /**
     * @param layout the layout to set
     */
    public void setLayout(String layout) {
        this.layout = layout;
    }
    
    
}
