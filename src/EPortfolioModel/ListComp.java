/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioModel;

import java.util.ArrayList;


/**
 *
 * @author KhaledAshraf
 */
public class ListComp extends Component {
    //private String listItem;
    private ArrayList<String> listArray;
    private boolean isSelected;
    
    public ListComp() {
        type = "List";
        isSelected = false;
        //listItem = "";
        listArray = new ArrayList<String>();
    }
    
    public String getType() {
        return type;
    }

    /**
     * @return the listItem
     */
//    public String getListItem() {
//        return listItem;
//    }
//
//    /**
//     * @param listItem the listItem to set
//     */
//    public void setListItem(String listItem) {
//        this.listItem = listItem;
//    }

    /**
     * @return the listArray
     */
    public ArrayList<String> getListArray() {
        return listArray;
    }

    /**
     * @param listArray the listArray to set
     */
    public void setListArray(ArrayList<String> listArray) {
        this.listArray = listArray;
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    /**
     * @param isSelected the isSelected to set
     */
    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
