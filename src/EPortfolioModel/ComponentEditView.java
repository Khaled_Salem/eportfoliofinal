/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package EPortfolioModel;

import EPortfolioView.EPortfolioView;
import EPortfolioView.ImageCurrentView;
import static EPortfolioView.StartupConstants.CSS_ALERT;
import static EPortfolioView.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static EPortfolioView.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import static EPortfolioView.StartupConstants.STYLE_SHEET_UI;
import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author KhaledAshraf
 */
public class ComponentEditView extends HBox {
    private Component component;
    
    //Highlight Slides
    PageModel componentSelect;
    
    //Updating buttons
    EPortfolioView ui;
    
    public ComponentEditView(Component initComp, PageModel pageModel) {
        
        component = initComp;
        
        componentSelect = pageModel;
        
        ui = pageModel.getUi();
        
        if(component.getType().equals("Paragraph")) {
            Label parLabel = new Label("Paragraph: ");
            Label fontLabel = new Label(", Font: ");
            Label fontSize = new Label(", Font Size: ");
            Label parText = new Label(((ParagraphComp)component).getText());
            Label parFont = new Label(((ParagraphComp)component).getFont());
            Label parFSize = new Label(((ParagraphComp)component).getFontSize());
            getChildren().add(parLabel);
            getChildren().add(parText);
            getChildren().add(fontLabel);
            getChildren().add(parFont);
            getChildren().add(fontSize);
            getChildren().add(parFSize);
            
            setOnMouseClicked(a -> {
                try {
                    MouseButton click = a.getButton();
                    if(click.name().equals("PRIMARY")) {
                        ui.getAddSlide().setDisable(true);
                        ui.getRemoveSlide().setDisable(true);
                        ui.getMoveUp().setDisable(true);
                        ui.getMoveDown().setDisable(true);
                        Stage parStage = new Stage();
                        parStage.setTitle("Edit Paragraph");
                        Scene parScene = new Scene(new VBox(20), 400, 270);
                        VBox parVbox = (VBox) parScene.getRoot();
                        parScene.getStylesheets().add(STYLE_SHEET_UI);
                        parVbox.getStyleClass().add(CSS_ALERT);
                        parVbox.setPadding(new Insets(5, 5, 5, 5));
                        ComboBox parComboBox = new ComboBox();
                        parComboBox.getItems().addAll("Times New Roman", "Arial", "Brush Script MT");
                        parComboBox.setValue(parComboBox.getItems().get(0));
                        TextArea editParagraph = new TextArea();
                        TextField editFSize = new TextField();
                        editParagraph.setMaxWidth(Double.MAX_VALUE);
                        editParagraph.setMaxHeight(Double.MAX_VALUE);
                        editFSize.setMaxWidth(Double.MAX_VALUE);
                        editFSize.setMaxHeight(Double.MAX_VALUE);
                        editParagraph.setWrapText(true);
                        editParagraph.setText(((ParagraphComp)component).getText());
                        editFSize.setText(((ParagraphComp)component).getFontSize());
                        for(int i = 0; i < parComboBox.getItems().size(); i++) {
                            if(((String)parComboBox.getItems().get(i)).equals(((ParagraphComp)component).getFont()))
                                parComboBox.setValue(parComboBox.getItems().get(i));
                        }
                        
                        Button parOkButton = new Button("Okay");
                        parOkButton.setOnAction(b -> {
                            if(ui.isNumber(editFSize.getText())) {
                                ((ParagraphComp)component).setText(editParagraph.getText());
                                ((ParagraphComp)component).setFont((String)parComboBox.getValue());
                                ((ParagraphComp)component).setFontSize(editFSize.getText());
                                ui.reloadEPortfolio(componentSelect);
                                // selectedBox.getChildren().add(parTextBox);
                                parStage.close();
                            }
                        });
                        parVbox.getChildren().addAll(parComboBox, editParagraph, editFSize, parOkButton);
                        parStage.setScene(parScene);
                        parStage.show();
                    }
                    else if (click.name().equals("SECONDARY")) {
                        ui.getRemoveSlide().setDisable(false);
                        ui.getRemoveSlide().setOnAction( f -> {
                            componentSelect.getComponents().remove(component);
                            ui.reloadEPortfolio(componentSelect);
                            ui.getRemoveSlide().setDisable(true);
                        });
                    }
                }catch(Exception e) {
                    
                }
            });
        }
        else if(component.getType().equals("Header")) {
            Label headerLabel = new Label("Header: ");
            Label fontLabel = new Label(", Font: ");
            Label fontSize = new Label(", Font Size: ");
            Label headerText = new Label(((HeaderComp)component).getText());
            Label headerFont = new Label(((HeaderComp)component).getFont());
            Label headerFSize = new Label(((HeaderComp)component).getFontSize());
            getChildren().add(headerLabel);
            getChildren().add(headerText);
            getChildren().add(fontLabel);
            getChildren().add(headerFont);
            getChildren().add(fontSize);
            getChildren().add(headerFSize);
            
            setOnMouseClicked(a -> {
                try {
                    MouseButton click = a.getButton();
                    if(click.name().equals("PRIMARY")) {
                        ui.getAddSlide().setDisable(true);
                        ui.getRemoveSlide().setDisable(true);
                        ui.getMoveUp().setDisable(true);
                        ui.getMoveDown().setDisable(true);
                        Stage headerStage = new Stage();
                        headerStage.setTitle("Edit Header");
                        Scene headerScene = new Scene(new VBox(20), 400, 270);
                        VBox headerVbox = (VBox) headerScene.getRoot();
                        headerScene.getStylesheets().add(STYLE_SHEET_UI);
                        headerVbox.getStyleClass().add(CSS_ALERT);
                        headerVbox.setPadding(new Insets(5, 5, 5, 5));
                        ComboBox headerComboBox = new ComboBox();
                        headerComboBox.getItems().addAll("Times New Roman", "Arial", "Brush Script MT");
                        headerComboBox.setValue(headerComboBox.getItems().get(0));
                        TextArea editHeader = new TextArea();
                        TextField editFSize = new TextField();
                        editHeader.setMaxWidth(Double.MAX_VALUE);
                        editHeader.setMaxHeight(Double.MAX_VALUE);
                        editFSize.setMaxWidth(Double.MAX_VALUE);
                        editFSize.setMaxHeight(Double.MAX_VALUE);
                        editHeader.setWrapText(true);
                        editHeader.setText(((HeaderComp)component).getText());
                        editFSize.setText(((HeaderComp)component).getFontSize());
                        for(int i = 0; i < headerComboBox.getItems().size(); i++) {
                            if(((String)headerComboBox.getItems().get(i)).equals(((HeaderComp)component).getFont()))
                                headerComboBox.setValue(headerComboBox.getItems().get(i));
                        }
                        Button headerOkButton = new Button("Okay");
                        headerOkButton.setOnAction(b -> {
                            if(ui.isNumber(editFSize.getText())) {
                                ((HeaderComp)component).setText(editHeader.getText());
                                ((HeaderComp)component).setFont((String)headerComboBox.getValue());
                                ((HeaderComp)component).setFontSize(editFSize.getText());
                                ui.reloadEPortfolio(componentSelect);
                                headerStage.close();
                            }
                        });
                        headerVbox.getChildren().addAll(headerComboBox, editHeader, editFSize, headerOkButton);
                        headerStage.setScene(headerScene);
                        headerStage.show();
                    }
                    else if(click.name().equals("SECONDARY")) {
                        ui.getRemoveSlide().setDisable(false);
                        ui.getRemoveSlide().setOnAction( f -> {
                            componentSelect.getComponents().remove(component);
                            ui.reloadEPortfolio(componentSelect);
                            ui.getRemoveSlide().setDisable(true);
                        });
                    }
                }catch(Exception e) {
                    
                }
            });
        }
        
        else if(component.getType().equals("Image")) {
            Label imageLabel = new Label("Image: ");
            Label labelWidth = new Label(", Width: ");
            Label labelHeight = new Label(", Height: ");
            Label labelCaption = new Label(", Caption: ");
            Label labelPos = new Label(", Float: ");
            Label imageFileName = new Label(((ImageComp)component).getImgFileName());
            Label imageWidth = new Label(String.valueOf(((ImageComp)component).getWidth()));
            Label imageHeight = new Label(String.valueOf(((ImageComp)component).getHeight()));
            Label imageCaption = new Label(((ImageComp)component).getCaption());
            Label imagePos = new Label(((ImageComp)component).getPosition());
            getChildren().add(imageLabel);
            getChildren().add(imageFileName);
            getChildren().add(labelWidth);
            getChildren().add(imageWidth);
            getChildren().add(labelHeight);
            getChildren().add(imageHeight);
            getChildren().add(labelCaption);
            getChildren().add(imageCaption);
            getChildren().add(labelPos);
            getChildren().add(imagePos);
            
            setOnMouseClicked(e -> {
                MouseButton click = e.getButton();
                if(click.name().equals("PRIMARY")) {
                    ui.getAddSlide().setDisable(true);
                    ui.getRemoveSlide().setDisable(true);
                    ui.getMoveUp().setDisable(true);
                    ui.getMoveDown().setDisable(true);
                    Stage newStage = new Stage();
                    newStage.setTitle("Add Image");
                    GridPane grid = new GridPane();
                    Button browse = new Button("Browse");
                    Button edit = new Button("Edit");
                    Scene scene = new Scene(grid, 450, 250);
                    scene.getStylesheets().add(STYLE_SHEET_UI);
                    grid.getStyleClass().add(CSS_ALERT);
                    grid.setHgap(10);
                    grid.setVgap(10);
                    grid.setPadding(new Insets(20, 150, 10, 10));
                    
                    TextField width = new TextField();
                    width.setText(String.valueOf(((ImageComp)component).getWidth()));
                    TextField height = new TextField();
                    height.setText(String.valueOf(((ImageComp)component).getHeight()));
                    TextField caption = new TextField();
                    caption.setText(((ImageComp)component).getCaption());
                    TextField position = new TextField();
                    position.setText(((ImageComp)component).getPosition());
                    
                    
                    grid.add(new Label("Width:"), 0, 0);
                    grid.add(width, 2, 0);
                    grid.add(new Label("Height:"), 0, 1);
                    grid.add(height, 2, 1);
                    grid.add(new Label("Caption:"), 0, 2);
                    grid.add(caption, 2, 2);
                    grid.add(new Label("Float:"), 0, 3);
                    grid.add(position, 2, 3);
                    grid.add(browse, 0, 4);
                    grid.add(edit, 2, 4);
                    
                    newStage.setScene(scene);
                    newStage.show();
                    
                    browse.setOnAction(f-> {
                        FileChooser fileChooser = new FileChooser();
                        FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
                        FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                        FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
                        fileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
                        File selectedFile = fileChooser.showOpenDialog(null);
                        if(selectedFile != null && ui.isNumber(width.getText()) && ui.isNumber(height.getText())) {
                            ((ImageComp)component).setWidth(Integer.valueOf(width.getText()));
                            ((ImageComp)component).setHeight(Integer.valueOf(height.getText()));
                            ((ImageComp)component).setCaption(caption.getText());
                            ((ImageComp)component).setPosition(position.getText());
                            ((ImageComp)component).setImgFileName(selectedFile.getName());
                            ((ImageComp)component).setImgFilePath(selectedFile.getAbsolutePath());
                            ui.reloadEPortfolio(componentSelect);
                            newStage.close();
                        }
                    });
                    
                    edit.setOnAction(f-> {
                        if(ui.isNumber(width.getText()) && ui.isNumber(height.getText())) {
                            ((ImageComp)component).setWidth(Integer.valueOf(width.getText()));
                            ((ImageComp)component).setHeight(Integer.valueOf(height.getText()));
                            ((ImageComp)component).setCaption(caption.getText());
                            ((ImageComp)component).setPosition(position.getText());
                            ui.reloadEPortfolio(componentSelect);
                            newStage.close();
                        }
                    });
                    
                }
                else if(click.name().equals("SECONDARY")) {
                    ui.getRemoveSlide().setDisable(false);
                    ui.getRemoveSlide().setOnAction( f -> {
                        componentSelect.getComponents().remove(component);
                        ui.reloadEPortfolio(componentSelect);
                        ui.getRemoveSlide().setDisable(true);
                    });
                }
            });
        }
        else if(component.getType().equals("BannerImg")) {
            Label bannerLabel = new Label("Banner: ");
            Label bannerImg = new Label(((BannerComp)component).getFileName());
            getChildren().add(bannerLabel);
            getChildren().add(bannerImg);
            ui.getBannerImg().setDisable(true);
            
            setOnMouseClicked(e -> {
                MouseButton click = e.getButton();
                if(click.name().equals("PRIMARY")) {
                    ui.getAddSlide().setDisable(true);
                    ui.getRemoveSlide().setDisable(true);
                    ui.getMoveUp().setDisable(true);
                    ui.getMoveDown().setDisable(true);
                    FileChooser fileChooser = new FileChooser();
                    File selectedFile = fileChooser.showOpenDialog(null);
                    if(selectedFile != null) {
                        ((BannerComp)component).setFileName(selectedFile.getName());
                        ((BannerComp)component).setImgFilePath(selectedFile.getAbsolutePath());
                        ui.getBannerImg().setDisable(true);
                        ui.reloadEPortfolio(componentSelect);
                    }
                }
                else if (click.name().equals("SECONDARY")) {
                    ui.getRemoveSlide().setDisable(false);
                    ui.getRemoveSlide().setOnAction( f -> {
                        componentSelect.getComponents().remove(component);
                        ui.reloadEPortfolio(componentSelect);
                        ui.getRemoveSlide().setDisable(true);
                        ui.getBannerImg().setDisable(false);
                    });
                }
            });
        }
        else if(component.getType().equals("Footer")) {
            Label footerLabel = new Label("Footer: ");
            Label footerText = new Label(((FooterComp)component).getText());
            Label fontLabel = new Label(", Font: ");
            Label footerFont = new Label(((FooterComp)component).getFont());
            Label fontSizeLabel = new Label(", Font Size: ");
            Label fontSize = new Label(((FooterComp)component).getFontSize());
            getChildren().add(footerLabel);
            getChildren().add(footerText);
            getChildren().add(fontLabel);
            getChildren().add(footerFont);
            getChildren().add(fontSizeLabel);
            getChildren().add(fontSize);
            
            setOnMouseClicked(e -> {
                try {
                    MouseButton click = e.getButton();
                    if(click.name().equals("PRIMARY")) {
                        ui.getAddSlide().setDisable(true);
                        ui.getRemoveSlide().setDisable(true);
                        ui.getMoveUp().setDisable(true);
                        ui.getMoveDown().setDisable(true);
                        Stage newStage = new Stage();
                        newStage.setTitle("Add Footer");
                        Scene scene = new Scene(new VBox(20), 400, 270);
                        VBox box = (VBox) scene.getRoot();
                        scene.getStylesheets().add(STYLE_SHEET_UI);
                        box.getStyleClass().add(CSS_ALERT);
                        box.setPadding(new Insets(5, 5, 5, 5));
                        ComboBox comboBox = new ComboBox();
                        comboBox.getItems().addAll("Times New Roman", "Arial", "Brush Script MT");
                        comboBox.setValue(comboBox.getItems().get(0));
                        TextArea footer = new TextArea();
                        TextField fSize = new TextField();
                        footer.setMaxWidth(Double.MAX_VALUE);
                        footer.setMaxHeight(Double.MAX_VALUE);
                        fSize.setMaxHeight(Double.MAX_VALUE);
                        footer.setWrapText(true);
                        footer.setText(((FooterComp)component).getText());
                        fSize.setText(((FooterComp)component).getFontSize());
                        for(int i = 0; i < comboBox.getItems().size(); i++) {
                            if(((String)comboBox.getItems().get(i)).equals(((FooterComp)component).getFont()))
                                comboBox.setValue(comboBox.getItems().get(i));
                        }
                        Button okay = new Button("Okay");
                        
                        okay.setOnAction(f -> {
                            if(ui.isNumber(fSize.getText())) {
                                ((FooterComp)component).setText(footer.getText());
                                ((FooterComp)component).setFont((String)comboBox.getValue());
                                ((FooterComp)component).setFontSize(fSize.getText());
                                ui.reloadEPortfolio(componentSelect);
                                newStage.close();
                            }
                        });
                        
                        box.getChildren().addAll(comboBox, footer, fSize, okay);
                        
                        newStage.setScene(scene);
                        newStage.show();
                    }
                    else if(click.name().equals("SECONDARY")) {
                        ui.getRemoveSlide().setDisable(false);
                        ui.getRemoveSlide().setOnAction( f -> {
                            componentSelect.getComponents().remove(component);
                            ui.reloadEPortfolio(componentSelect);
                            ui.getRemoveSlide().setDisable(true);
                        });
                    }
                }catch(Exception ex) {
                    
                }
            });
        }
        
        else if(component.getType().equals("Video")) {
            Label videoLabel = new Label("Video: ");
            Label labelWidth = new Label(", Width: ");
            Label labelHeight = new Label(", Height: ");
            Label labelCaption = new Label(", Caption: ");
            Label labelPos = new Label(", Float: ");
            Label videoFileName = new Label(((VideoComp)component).getVideoFileName());
            Label videoWidth = new Label(String.valueOf(((VideoComp)component).getWidth()));
            Label videoHeight = new Label(String.valueOf(((VideoComp)component).getHeight()));
            Label videoCaption = new Label(((VideoComp)component).getCaption());
            Label videoPos = new Label(((VideoComp)component).getPosition());
            getChildren().add(videoLabel);
            getChildren().add(videoFileName);
            getChildren().add(labelWidth);
            getChildren().add(videoWidth);
            getChildren().add(labelHeight);
            getChildren().add(videoHeight);
            getChildren().add(labelCaption);
            getChildren().add(videoCaption);
            getChildren().add(labelPos);
            getChildren().add(videoPos);
            
            setOnMouseClicked(e -> {
                try {
                    MouseButton click = e.getButton();
                    if(click.name().equals("PRIMARY")) {
                        ui.getAddSlide().setDisable(true);
                        ui.getRemoveSlide().setDisable(true);
                        ui.getMoveUp().setDisable(true);
                        ui.getMoveDown().setDisable(true);
                        Stage newStage = new Stage();
                        newStage.setTitle("Add Video");
                        GridPane grid = new GridPane();
                        Button browse = new Button("Browse");
                        Button edit = new Button("Edit");
                        Scene scene = new Scene(grid, 450, 250);
                        scene.getStylesheets().add(STYLE_SHEET_UI);
                        grid.getStyleClass().add(CSS_ALERT);
                        grid.setHgap(10);
                        grid.setVgap(10);
                        grid.setPadding(new Insets(20, 150, 10, 10));
                        
                        TextField width = new TextField();
                        width.setText(String.valueOf(((VideoComp)component).getWidth()));
                        TextField height = new TextField();
                        height.setText(String.valueOf(((VideoComp)component).getHeight()));
                        TextField caption = new TextField();
                        caption.setText(((VideoComp)component).getCaption());
                        TextField position = new TextField();
                        position.setText(((VideoComp)component).getPosition());
                        
                        
                        grid.add(new Label("Width:"), 0, 0);
                        grid.add(width, 2, 0);
                        grid.add(new Label("Height:"), 0, 1);
                        grid.add(height, 2, 1);
                        grid.add(new Label("Caption:"), 0, 2);
                        grid.add(caption, 2, 2);
                        grid.add(new Label("Float:"), 0, 3);
                        grid.add(position, 2, 3);
                        grid.add(browse, 0, 4);
                        grid.add(edit, 2, 4);
                        
                        newStage.setScene(scene);
                        newStage.show();
                        
                        browse.setOnAction(f-> {
                            FileChooser fileChooser = new FileChooser();
                            FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
                            fileChooser.getExtensionFilters().addAll(mp4Filter);
                            File selectedFile = fileChooser.showOpenDialog(null);
                            if(selectedFile != null && ui.isNumber(width.getText()) && ui.isNumber(height.getText())) {
                                ((VideoComp)component).setWidth(Integer.valueOf(width.getText()));
                                ((VideoComp)component).setHeight(Integer.valueOf(height.getText()));
                                ((VideoComp)component).setCaption(caption.getText());
                                ((VideoComp)component).setPosition(position.getText());
                                ((VideoComp)component).setVideoFileName(selectedFile.getName());
                                ((VideoComp)component).setVideoFilePath(selectedFile.getAbsolutePath());
                                ui.reloadEPortfolio(componentSelect);
                                newStage.close();
                            }
                        });
                        
                        edit.setOnAction(f-> {
                            ((VideoComp)component).setWidth(Integer.valueOf(width.getText()));
                            ((VideoComp)component).setHeight(Integer.valueOf(height.getText()));
                            ((VideoComp)component).setCaption(caption.getText());
                            ((VideoComp)component).setPosition(position.getText());
                            ui.reloadEPortfolio(componentSelect);
                            newStage.close();
                        });
                        
                        
                    }
                    else if(click.name().equals("SECONDARY")) {
                        ui.getRemoveSlide().setDisable(false);
                        ui.getRemoveSlide().setOnAction( f -> {
                            componentSelect.getComponents().remove(component);
                            ui.reloadEPortfolio(componentSelect);
                            ui.getRemoveSlide().setDisable(true);
                        });
                    }
                }catch(Exception ex) {
                    
                }
            });
        }
        
        else if(component.getType().equals("List")) {
            ArrayList arrayList = ((ListComp)component).getListArray();
            int arraySize = arrayList.size();
            VBox listBox = new VBox(10);
            for(int i = 0; i < arraySize; i++) {
                Label listLabel = new Label("List " + (i + 1) + ": ");
                String text = (String) arrayList.get(i);
                Label listText = new Label(text);
                HBox innerBox = new HBox();
                innerBox.getChildren().add(listLabel);
                innerBox.getChildren().add(listText);
                listBox.getChildren().add(innerBox);
            }
            getChildren().add(listBox);
            
            listBox.setOnMouseClicked(e -> {
                Stage newStage = new Stage();
                Scene scene = new Scene(new VBox(20), 400, 450);
                VBox box = (VBox) scene.getRoot();
                HBox buttons = new HBox(15);
                scene.getStylesheets().add(STYLE_SHEET_UI);
                box.getStyleClass().add(CSS_ALERT);
                box.setPadding(new Insets(5, 5, 5, 5));
                Button okay = new Button("Okay");
                Button addItem = new Button("Add List Item");
                buttons.getChildren().addAll(okay, addItem);
                
                for(int i=0; i< arraySize; i++) {
                    newStage.setTitle("Edit List Items");
                    Label listItemLabel = new Label("List " + (i + 1));
                    String text = (String) arrayList.get(i);
                    TextField listItem = new TextField(text);
                    HBox listItemBox = new HBox(15);
                    listItemBox.getChildren().add(listItemLabel);
                    listItemBox.getChildren().add(listItem);
                    box.getChildren().add(listItemBox);
                }
                
                okay.setOnAction(f -> {
                    int j = 0;
                    for(int i = 0; i < box.getChildren().size() - 1; i++) {
                        HBox hboxx = (HBox) box.getChildren().get(i);
                        TextField textt = (TextField) hboxx.getChildren().get(1);
                        if(textt.getText().length() == 0) {
                            arrayList.remove(j);
                        }
                        else {
                            arrayList.set(j, textt.getText());
                            j++;
                        }
                    }
                    ui.reloadEPortfolio(componentSelect);
                    newStage.close();
                    
                });
                
                addItem.setOnMouseClicked(d -> {
                    newStage.close();
                    TextInputDialog ePfLists = new TextInputDialog();
                    ePfLists.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                    ePfLists.getDialogPane().getStyleClass().add(CSS_ALERT);
                    ePfLists.setTitle("List Option");
                    ePfLists.setHeaderText(null);
                    ePfLists.setContentText("How many list items would you like to add?");
                    Optional<String> answer = ePfLists.showAndWait();
                    
                    Stage addStage = new Stage();
                    Scene addScene = new Scene(new VBox(20), 400, 600);
                    VBox addBox = (VBox) addScene.getRoot();
                    addScene.getStylesheets().add(STYLE_SHEET_UI);
                    addBox.getStyleClass().add(CSS_ALERT);
                    addBox.setPadding(new Insets(5, 5, 5, 5));
                    Button addOkay = new Button("Okay");
                    
                    for(int i=0; i< arraySize; i++) {
                        addStage.setTitle("Edit List Items");
                        Label listItemLabel = new Label("List " + (i + 1));
                        String text = (String) arrayList.get(i);
                        TextField listItem = new TextField(text);
                        HBox listItemBox = new HBox(15);
                        listItemBox.getChildren().add(listItemLabel);
                        listItemBox.getChildren().add(listItem);
                        addBox.getChildren().add(listItemBox);
                    }
                    
                    for(int i=arraySize; i< Integer.valueOf(answer.get()) + arraySize; i++) {
                        Label listItemLabel = new Label("List " + (i + 1));
                        TextField listItem = new TextField();
                        listItem.setPromptText("Enter your text here!");
                        HBox listItemBox = new HBox(15);
                        listItemBox.getChildren().add(listItemLabel);
                        listItemBox.getChildren().add(listItem);
                        addBox.getChildren().add(listItemBox);
                    }
                    
                    addBox.getChildren().add(addOkay);
                    
                    addOkay.setOnAction(f -> {
                        int j = 0;
                        for(int i = 0; i < addBox.getChildren().size() - 1; i++) {
                            HBox hboxx = (HBox) addBox.getChildren().get(i);
                            TextField textt = (TextField) hboxx.getChildren().get(1);
                            if(textt.getText().length() == 0) {
                                arrayList.remove(j);
                            }
                            else {
                                if(j < arraySize) {
                                    arrayList.set(j, textt.getText());
                                    j++;
                                }
                                else
                                    arrayList.add(textt.getText());
                            }
                        }
                        ui.reloadEPortfolio(componentSelect);
                        addStage.close();
                        
                    });
                    
                    addStage.setScene(addScene);
                    addStage.show();
                });
                
                box.getChildren().add(buttons);
                newStage.setScene(scene);
                newStage.show();
            });
        }
        
        else if(component.getType().equals("Slideshow")) {
            ArrayList arrayPath = ((SlideshowComp)component).getImages();
            ArrayList arrayCaps = ((SlideshowComp)component).getCaptions();
            int arraySize = arrayPath.size();
            VBox box = new VBox(10);
            for(int i = 0; i < arraySize; i++) {
                String imagePath = (String) arrayPath.get(i);
                String cap = (String) arrayCaps.get(i);
                Label caption = new Label(cap);
                Image defaultImg = new Image(imagePath);
                ImageView slideBox = new ImageView(defaultImg);
                slideBox.setFitHeight(150);
                slideBox.setFitWidth(150);
                HBox slideItemBox = new HBox(30);
                slideItemBox.getChildren().add(slideBox);
                slideItemBox.getChildren().add(caption);
                box.getChildren().add(slideItemBox);
            }
            getChildren().add(box);
//            VBox thisVbox = (VBox)getChildren().get(0);
//            HBox thisHbox = (HBox)thisVbox.getChildren().get(0);
//            ImageView thisImageView = (ImageView)thisHbox.getChildren().get(0);
            
            box.setOnMouseClicked(e -> {
                MouseButton click = e.getButton();
                if(click.name().equals("PRIMARY")) {
                    ui.getAddSlide().setDisable(true);
                    ui.getRemoveSlide().setDisable(true);
                    ui.getMoveUp().setDisable(true);
                    ui.getMoveDown().setDisable(true);
                    Stage newStage = new Stage();
                    Scene scene = new Scene(new VBox(20), 600, 600);
                    VBox vBox = (VBox) scene.getRoot();
                    HBox buttons = new HBox(15);
                    scene.getStylesheets().add(STYLE_SHEET_UI);
                    vBox.getStyleClass().add(CSS_ALERT);
                    vBox.setPadding(new Insets(5, 5, 5, 5));
                    Button okay = new Button("Okay");
                    buttons.getChildren().addAll(okay);
                    
                    for(int i=0; i< arraySize; i++) {
                        newStage.setTitle("Edit SlideShow");
                        String imagePath = (String) arrayPath.get(i);
                        String cap = (String) arrayCaps.get(i);
                        TextField caption = new TextField(cap);
                        caption.setPromptText("Empty!");
                        Image defaultImg = new Image(imagePath);
                        ImageCurrentView slideBox = new ImageCurrentView(defaultImg, i);
                        slideBox.getCurrentImageView().setFitHeight(150);
                        slideBox.getCurrentImageView().setFitWidth(150);
                        HBox slideItemBox = new HBox(30);
                        slideItemBox.getChildren().add(slideBox.getCurrentImageView());
                        slideItemBox.getChildren().add(caption);
                        vBox.getChildren().add(slideItemBox);
                        
                        slideBox.getCurrentImageView().setOnMouseClicked(f -> {
                            FileChooser imageChooser = new FileChooser();
                            imageChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
                            FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
                            FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                            FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
                            imageChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
                            File file = imageChooser.showOpenDialog(null);
                            if (file != null) {
                                String path = file.getAbsolutePath().substring(0, file.getPath().indexOf(file.getName()));
                                String fileName = file.getName();
                                String wholeFile = "file:" + path + fileName;
                                arrayPath.set(slideBox.getImageNumber(), wholeFile);
                            }
                            else {
                            }
                        });
                    }
                    okay.setOnAction(d -> {
                        for(int i = 0; i < vBox.getChildren().size() - 1; i++) {
                            HBox hboxx = (HBox) vBox.getChildren().get(i);
                            TextField textt = (TextField) hboxx.getChildren().get(1);
                            arrayCaps.set(i, textt.getText());
                        }
                        ui.reloadEPortfolio(componentSelect);
                        ui.getSaveEPfButton().setDisable(false);
                        ui.getSaveAsEPfButton().setDisable(false);
                        newStage.close();
                    });
                    
                    vBox.getChildren().add(buttons);
                    newStage.setScene(scene);
                    newStage.show();
                }
                else if(click.name().equals("SECONDARY")) {
                    ((SlideshowComp)component).setIsSelected(true);
                    ui.getAddSlide().setDisable(false);
                    ui.getRemoveSlide().setDisable(false);
                    ui.getMoveUp().setDisable(false);
                    ui.getMoveDown().setDisable(false);
                    Button addSlide = ui.getAddSlide();
                    Button removeSlide = ui.getRemoveSlide();
                    Button moveUp = ui.getMoveUp();
                    Button moveDown = ui.getMoveDown();
                    String imagePath = "file:" + PATH_SLIDE_SHOW_IMAGES + DEFAULT_SLIDE_IMAGE;
                    addSlide.setOnAction(a ->{
                        TextField caption = new TextField();
                        caption.setPromptText("Empty");
                        ((SlideshowComp)component).getImages().add(imagePath);
                        ((SlideshowComp)component).getCaptions().add(caption.getText());
                        Label newCaption = new Label(caption.getText());
                        Image defaultImg = new Image(imagePath);
                        ImageView slideBox = new ImageView(defaultImg);
                        slideBox.setFitHeight(150);
                        slideBox.setFitWidth(150);
                        HBox slideItemBox = new HBox(30);
                        slideItemBox.getChildren().add(slideBox);
                        slideItemBox.getChildren().add(newCaption);
                        box.getChildren().add(slideItemBox);
                        ui.updateButtons();
                        ui.getSaveEPfButton().setDisable(false);
                        ui.getSaveAsEPfButton().setDisable(false);
                        ui.reloadEPortfolio(componentSelect);
                        
                    });
                    
                    removeSlide.setOnAction(b ->{
                        TextInputDialog removeDialog = new TextInputDialog();
                        removeDialog.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                        removeDialog.getDialogPane().getStyleClass().add(CSS_ALERT);
                        removeDialog.setTitle("Remove Slide");
                        removeDialog.setHeaderText(null);
                        removeDialog.setContentText("Which slide number would you like to remove?");
                        Optional<String> slide = removeDialog.showAndWait();
                        int slideNumber = Integer.valueOf(slide.get());
                        ((SlideshowComp)component).getImages().remove(slideNumber - 1);
                        ((SlideshowComp)component).getCaptions().remove(slideNumber - 1);
                        ui.updateButtons();
                        ui.getSaveEPfButton().setDisable(false);
                        ui.getSaveAsEPfButton().setDisable(false);
                        ui.reloadEPortfolio(componentSelect);
                    });
                    
                    moveUp.setOnAction(c -> {
                        TextInputDialog removeDialog = new TextInputDialog();
                        removeDialog.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                        removeDialog.getDialogPane().getStyleClass().add(CSS_ALERT);
                        removeDialog.setTitle("Remove Slide");
                        removeDialog.setHeaderText(null);
                        removeDialog.setContentText("Which slide number would you like to move up?");
                        Optional<String> slide = removeDialog.showAndWait();
                        int slideNumber = Integer.valueOf(slide.get());
                        if(slideNumber == 1) {
                            
                        }
                        else {
                            ui.updateButtons();
                            ui.getSaveEPfButton().setDisable(false);
                            ui.getSaveAsEPfButton().setDisable(false);
                            String tmpImg = ((SlideshowComp)component).getImages().get(slideNumber -1);
                            String tmpcap = ((SlideshowComp)component).getCaptions().get(slideNumber - 1);
                            String string = ((SlideshowComp)component).getImages().get(slideNumber - 2);
                            String otherString = ((SlideshowComp)component).getCaptions().get(slideNumber - 2);
                            ((SlideshowComp)component).getImages().set(slideNumber - 1, string);
                            ((SlideshowComp)component).getCaptions().set(slideNumber - 1, otherString);
                            ((SlideshowComp)component).getImages().set(slideNumber - 2, tmpImg);
                            ((SlideshowComp)component).getCaptions().set(slideNumber - 2, tmpcap);
                            ui.reloadEPortfolio(componentSelect);
                        }
                    });
                    
                    moveDown.setOnAction(c -> {
                        TextInputDialog removeDialog = new TextInputDialog();
                        removeDialog.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                        removeDialog.getDialogPane().getStyleClass().add(CSS_ALERT);
                        removeDialog.setTitle("Remove Slide");
                        removeDialog.setHeaderText(null);
                        removeDialog.setContentText("Which slide number would you like to move down?");
                        Optional<String> slide = removeDialog.showAndWait();
                        int slideNumber = Integer.valueOf(slide.get());
                        if(slideNumber - 1 == arrayPath.size() - 1) {
                            
                        }
                        else {
                            ui.updateButtons();
                            ui.getSaveEPfButton().setDisable(false);
                            ui.getSaveAsEPfButton().setDisable(false);
                            String tmpImg = ((SlideshowComp)component).getImages().get(slideNumber - 1);
                            String tmpcap = ((SlideshowComp)component).getCaptions().get(slideNumber - 1);
                            String string = ((SlideshowComp)component).getImages().get(slideNumber);
                            String otherString = ((SlideshowComp)component).getCaptions().get(slideNumber);
                            ((SlideshowComp)component).getImages().set(slideNumber - 1, string);
                            ((SlideshowComp)component).getCaptions().set(slideNumber - 1, otherString);
                            ((SlideshowComp)component).getImages().set(slideNumber, tmpImg);
                            ((SlideshowComp)component).getCaptions().set(slideNumber, tmpcap);
                            ui.reloadEPortfolio(componentSelect);
                        }
                    });
                }
            });
        }
        
        else if(component.getType().equals("Color")) {
            Label colorLabel = new Label("Color: ");
            Label color = new Label(((ColorComp)component).getColor());
            componentSelect.setColor(color.getText());
            getChildren().add(colorLabel);
            getChildren().add(color);
            
            setOnMouseClicked(e -> {
                Stage newStage = new Stage();
                newStage.setTitle("ColorPicker");
                Scene scene = new Scene(new VBox(20), 400, 130);
                VBox box = (VBox) scene.getRoot();
                box.setPadding(new Insets(5, 5, 5, 5));
                Button okButton = new Button("Okay");
                
                final ColorPicker colorPicker = new ColorPicker();
                
                colorPicker.setValue(((ColorComp)component).getCurrentColor());
                
                final Text text = new Text("Looks Good!!");
                text.setFont(Font.font ("Verdana", 20));
                text.setFill(colorPicker.getValue());
                
                colorPicker.setOnAction((ActionEvent f) -> {
                    text.setFill(colorPicker.getValue());
                });
                
                okButton.setOnAction(f -> {
                    ((ColorComp)component).setColor(colorPicker.getValue().toString());
                    ((ColorComp)component).setCurrentColor(colorPicker.getValue());
                    componentSelect.setColor(colorPicker.getValue().toString());
                    ui.reloadEPortfolio(componentSelect);
                    newStage.close();
                });
                
                box.getChildren().addAll(colorPicker, text, okButton);
                
                newStage.setScene(scene);
                newStage.show();
            });
        }
        else if(component.getType().equals("Layout")) {
            Label layoutLabel = new Label("Layout: ");
            Label layout = new Label(((LayoutComp)component).getLayout());
            componentSelect.setLayout(layout.getText());
            getChildren().add(layoutLabel);
            getChildren().add(layout);
            
            setOnMouseClicked(e ->{
                Stage newStage = new Stage();
                newStage.setTitle("Select Layout");
                Scene scene = new Scene(new VBox(20), 400, 270);
                VBox box = (VBox) scene.getRoot();
                scene.getStylesheets().add(STYLE_SHEET_UI);
                box.getStyleClass().add(CSS_ALERT);
                box.setPadding(new Insets(5, 5, 5, 5));
                ComboBox comboBox = new ComboBox();
                comboBox.getItems().addAll("Select Layout","Layout 1", "Layout 2", "Layout 3", "Layout 4", "Layout 5", "Layout 6");
                for(int i = 0; i < comboBox.getItems().size(); i++) {
                    if(comboBox.getItems().get(i).equals(((LayoutComp)component).getLayout()))
                        comboBox.setValue(comboBox.getItems().get(i));
                }
                Button okButton = new Button("Okay");
                box.getChildren().add(comboBox);
                
                comboBox.setOnAction(d -> {
                    if(comboBox.getValue().toString().equals("Layout 1")) {
                        box.getChildren().clear();
                        box.getChildren().add(comboBox);
                        final Text text1 = new Text("Banner Image available!");
                        final Text text2 = new Text("Nav bar centered under Banner!");
                        text1.setFill(Color.WHITE);
                        text2.setFill(Color.WHITE);
                        text1.setFont(Font.font ("Verdana", 12));
                        text2.setFont(Font.font ("Verdana", 12));
                        box.getChildren().addAll(text1, text2, okButton);
                    }
                    else if(comboBox.getValue().toString().equals("Layout 2")) {
                        box.getChildren().clear();
                        box.getChildren().add(comboBox);
                        final Text text1 = new Text("Nav bar on top!");
                        final Text text2 = new Text("Banner Image under Nav bar!");
                        text1.setFill(Color.WHITE);
                        text2.setFill(Color.WHITE);
                        text1.setFont(Font.font ("Verdana", 12));
                        text2.setFont(Font.font ("Verdana", 12));
                        box.getChildren().addAll(text1, text2, okButton);
                    }
                    else if(comboBox.getValue().toString().equals("Layout 3")) {
                        box.getChildren().clear();
                        box.getChildren().add(comboBox);
                        final Text text1 = new Text("Nav bar on left!");
                        final Text text2 = new Text("No Banner Image!");
                        text1.setFill(Color.WHITE);
                        text2.setFill(Color.WHITE);
                        text1.setFont(Font.font ("Verdana", 12));
                        text2.setFont(Font.font ("Verdana", 12));
                        box.getChildren().addAll(text1, text2, okButton);
                    }
                    else if(comboBox.getValue().toString().equals("Layout 4")) {
                        box.getChildren().clear();
                        box.getChildren().add(comboBox);
                        final Text text1 = new Text("Nav bar centered on top!");
                        final Text text2 = new Text("Banner Image under Nav bar!");
                        text1.setFill(Color.WHITE);
                        text2.setFill(Color.WHITE);
                        text1.setFont(Font.font ("Verdana", 12));
                        text2.setFont(Font.font ("Verdana", 12));
                        box.getChildren().addAll(text1, text2, okButton);
                    }
                    else if(comboBox.getValue().toString().equals("Layout 5")) {
                        box.getChildren().clear();
                        box.getChildren().add(comboBox);
                        final Text text1 = new Text("Nav bar on top!");
                        final Text text2 = new Text("No Banner Image!");
                        text1.setFill(Color.WHITE);
                        text2.setFill(Color.WHITE);
                        text1.setFont(Font.font ("Verdana", 12));
                        text2.setFont(Font.font ("Verdana", 12));
                        box.getChildren().addAll(text1, text2, okButton);
                    }
                    else if(comboBox.getValue().toString().equals("Layout 6")) {
                        box.getChildren().clear();
                        box.getChildren().add(comboBox);
                        final Text text1 = new Text("Banner Image available!");
                        final Text text2 = new Text("Nav bar on left!");
                        text1.setFill(Color.WHITE);
                        text2.setFill(Color.WHITE);
                        text1.setFont(Font.font ("Verdana", 12));
                        text2.setFont(Font.font ("Verdana", 12));
                        box.getChildren().addAll(text1, text2, okButton);
                    }
                    else if(comboBox.getValue().toString().equals("Select Layout")) {
                        box.getChildren().clear();
                        box.getChildren().add(comboBox);
                    }
                });
                newStage.setScene(scene);
                newStage.show();
                
                
                okButton.setOnAction(f -> {
                    ((LayoutComp)component).setLayout(comboBox.getValue().toString());
                    componentSelect.setLayout(comboBox.getValue().toString());
                    ui.reloadEPortfolio(componentSelect);
                    newStage.close();
                });
                newStage.setScene(scene);
                newStage.show();
            });
            
            
        }
    }
}