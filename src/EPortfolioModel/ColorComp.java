/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioModel;

import javafx.scene.paint.Color;

/**
 *
 * @author KhaledAshraf
 */
public class ColorComp extends Component{
    private String color;
    private Color currentColor;
    
    public ColorComp(Color currentColor) {
        color = "";
        type = "Color";
        this.currentColor = currentColor;
    }
    
    public String getType() {
        return type;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the currentColor
     */
    public Color getCurrentColor() {
        return currentColor;
    }

    /**
     * @param currentColor the currentColor to set
     */
    public void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }
    
}
