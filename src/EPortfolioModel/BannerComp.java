/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioModel;

/**
 *
 * @author KhaledAshraf
 */
public class BannerComp extends Component {
    private String fileName;
    private boolean isSelected;
    private String imgFilePath;
    
    public BannerComp() {
        fileName = "";
        isSelected = false;
        type = "BannerImg";
    }

    
    public String getType() {
        return type;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    
    public boolean isIsSelected() {
        return isSelected;
    }

    /**
     * @param isSelected the isSelected to set
     */
    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * @return the imgFilePath
     */
    public String getImgFilePath() {
        return imgFilePath;
    }

    /**
     * @param imgFilePath the imgFilePath to set
     */
    public void setImgFilePath(String imgFilePath) {
        this.imgFilePath = imgFilePath;
    }
}
