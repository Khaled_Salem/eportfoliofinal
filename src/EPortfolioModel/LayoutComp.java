/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioModel;

/**
 *
 * @author KhaledAshraf
 */
public class LayoutComp extends Component {
    private String layout;
    
    public LayoutComp() {
        layout = "";
        type = "Layout";
    }
    
    public String getType() {
        return type;
    }

    /**
     * @return the layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * @param layout the layout to set
     */
    public void setLayout(String layout) {
        this.layout = layout;
    }
    
}
