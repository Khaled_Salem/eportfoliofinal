/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioModel;

/**
 *
 * @author KhaledAshraf
 */
public abstract class Component {
    
    String type;

    /**
     * @return the type
     */
    public abstract String getType();

    
}
