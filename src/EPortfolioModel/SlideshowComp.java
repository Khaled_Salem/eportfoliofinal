/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioModel;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author KhaledAshraf
 */
public class SlideshowComp extends Component {
    private ArrayList<String> images;
    private String imagePath;
    private String imageFileName;
    private ArrayList<String> captions;
    private boolean isSelected;
    
    public SlideshowComp() {
        type = "Slideshow";
        isSelected = false;
        imagePath = "";
        imageFileName = "";
        images = new ArrayList<String>();
        captions = new ArrayList<String>();
    }
    
    public String getType() {
        return type;
    }

    /**
     * @return the images
     */
    public ArrayList<String> getImages() {
        return images;
    }

    /**
     * @param images the images to set
     */
    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    /**
     * @return the captions
     */
    public ArrayList<String> getCaptions() {
        return captions;
    }

    /**
     * @param captions the captions to set
     */
    public void setCaptions(ArrayList<String> captions) {
        this.captions = captions;
    }

    /**
     * @return the imagePath
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * @param imagePath the imagePath to set
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * @return the imageFileName
     */
    public String getImageFileName() {
        return imageFileName;
    }

    /**
     * @param imageFileName the imageFileName to set
     */
    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }


    public boolean isIsSelected() {
       return isSelected;
    }

    /**
     * @param isSelected the isSelected to set
     */
    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
    
}
