/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioModel;

/**
 *
 * @author KhaledAshraf
 */
public class VideoComp extends Component {
    private String videoFileName;
    private String videoFilePath;
    private String caption;
    private int width;
    private int height;
    private String position;
    private boolean isSelected;
    
    public VideoComp() {
        videoFileName = "";
        type = "Video";
        isSelected = false;
        caption = "";
        width = 0;
        height = 0;
        position = "";
    }


    /**
     * @return the videoFileName
     */
    public String getVideoFileName() {
        return videoFileName;
    }

    /**
     * @param videoFileName the videoFileName to set
     */
    public void setVideoFileName(String videoFileName) {
        this.videoFileName = videoFileName;
    }

    /**
     * @return the caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * @param caption the caption to set
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    
    public String getType() {
        return type;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param positon the position to set
     */
    public void setPosition(String positon) {
        this.position = positon;
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    /**
     * @param isSelected the isSelected to set
     */
    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * @return the videoFilePath
     */
    public String getVideoFilePath() {
        return videoFilePath;
    }

    /**
     * @param videoFilePath the videoFilePath to set
     */
    public void setVideoFilePath(String videoFilePath) {
        this.videoFilePath = videoFilePath;
    }
}
