package EPortfolioView;

import EPortfolioModel.BannerComp;
import EPortfolioModel.ColorComp;
import EPortfolioModel.Component;
import EPortfolioModel.ComponentEditView;
import EPortfolioModel.FooterComp;
import EPortfolioModel.HeaderComp;
import EPortfolioModel.ImageComp;
import EPortfolioModel.LayoutComp;
import EPortfolioModel.ListComp;
import EPortfolioModel.PageModel;
import EPortfolioModel.ParagraphComp;
import EPortfolioModel.SlideshowComp;
import EPortfolioModel.VideoComp;
import static EPortfolioView.StartupConstants.CSS_ALERT;
import static EPortfolioView.StartupConstants.CSS_CLASS_EPF_EDIT_VBOX;
import static EPortfolioView.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static EPortfolioView.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static EPortfolioView.StartupConstants.CSS_FileToolbarPane;
import static EPortfolioView.StartupConstants.CSS_PAGE_WORKSPACE;
import static EPortfolioView.StartupConstants.CSS_TAB_PANE;
import static EPortfolioView.StartupConstants.CSS_UI;
import static EPortfolioView.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static EPortfolioView.StartupConstants.ICON_ADD_HEADER;
import static EPortfolioView.StartupConstants.ICON_ADD_HYPER;
import static EPortfolioView.StartupConstants.ICON_ADD_IMAGE;
import static EPortfolioView.StartupConstants.ICON_ADD_LIST;
import static EPortfolioView.StartupConstants.ICON_ADD_PAGE;
import static EPortfolioView.StartupConstants.ICON_ADD_PAR;
import static EPortfolioView.StartupConstants.ICON_ADD_SLIDE;
import static EPortfolioView.StartupConstants.ICON_ADD_SLIDESHOW;
import static EPortfolioView.StartupConstants.ICON_ADD_VIDEO;
import static EPortfolioView.StartupConstants.ICON_CHANGE_TITLE;
import static EPortfolioView.StartupConstants.ICON_COLOR_TEMP;
import static EPortfolioView.StartupConstants.ICON_EXIT;
import static EPortfolioView.StartupConstants.ICON_EXPORT_EPF;
import static EPortfolioView.StartupConstants.ICON_FOOTER_TEXT;
import static EPortfolioView.StartupConstants.ICON_LOAD_EPF;
import static EPortfolioView.StartupConstants.ICON_MOVE_DOWN;
import static EPortfolioView.StartupConstants.ICON_MOVE_UP;
import static EPortfolioView.StartupConstants.ICON_NEW_EPF;
import static EPortfolioView.StartupConstants.ICON_PAGE_FONT;
import static EPortfolioView.StartupConstants.ICON_REMOVE_PAGE;
import static EPortfolioView.StartupConstants.ICON_REMOVE_SLIDE;
import static EPortfolioView.StartupConstants.ICON_SAVE_AS_EPF;
import static EPortfolioView.StartupConstants.ICON_SAVE_EPF;
import static EPortfolioView.StartupConstants.ICON_SITE_VIEWER;
import static EPortfolioView.StartupConstants.ICON_SLCT_BANNER;
import static EPortfolioView.StartupConstants.ICON_SLCT_LAYOUT;
import static EPortfolioView.StartupConstants.ICON_STUDENT_NAME;
import static EPortfolioView.StartupConstants.PATH_ICONS;
import static EPortfolioView.StartupConstants.PATH_IMAGES;
import static EPortfolioView.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import static EPortfolioView.StartupConstants.STYLE_SHEET_UI;
import FileManager.EPortfolioFileManager;
import FileManager.FileController;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author KhaledAshraf
 */
public class EPortfolioView {
    
    //This is the stage and scene of the UI
    private Stage primaryStage;
    Scene primaryScene;
    
    //This is how the content will be organized in the UI
    BorderPane ePfPane;
    
    //This will be the File toolbar appearing at the top
    VBox fileToolbarPane;
    
    //This will be the Page Workspace
    private TabPane pageWorkspace;
    
    //Buttons in the File toolbar
    private Button newEPfButton;
    Button loadEPfButton;
    private Button saveEPfButton;
    private Button saveAsEPfButton;
    Button exportButton;
    Button exitEPfButton;
    
    //VBox for buttons editing each ePortfolio
    VBox ePfEditToolbar;
    
    //Boxes for page content
    FlowPane selectedPane;
    VBox selectedBox;
    
    //Component object
    private PageModel pageModel;
    private ObservableList<PageModel> pages = FXCollections.observableArrayList();;
    
    //VBox for the Slide show buttons
    HBox slideShowToolbar;
    
    //Buttons for the ePortfolio editting
    private Button addPage;
    Button removePage;
    Button pageTitle;
    private Button studentName;
    private Button selectBannerImg;
    Button footerTxt;
    private Button moveUp;
    private Button moveDown;
    private Button addSlide;
    private Button removeSlide;
    private Button bannerImg;
    Button slctLayout;
    Button colorTemp;
    Button siteViewer;
    Button addImg;
    Button addList;
    Button addParagraph;
    Button addHeader;
    Button addSlideshow;
    Button addVideo;
    Button addHyper;
    
    //Page counter
    public static int pageCounter = 1;
    // public static int listCount = 1;
    
    //Which tab is being selected
    Tab selectedTab;
    
    //File Controller for save/load
    private FileController fileController;
    EPortfolioFileManager fileManager;
    
    //Entries used for Page Font Dialog boxes.
    private final String [] arrayData = {"Times New Roman", "Arial", "Brush Script MT"};
    private List<String> dialogData;
    
    public EPortfolioView(EPortfolioFileManager initFileManager) {
        // FIRST HOLD ONTO THE FILE MANAGER
        fileManager = initFileManager;
    }
    
    
    public void startUI(Stage primStage, String windowTitle) {
        
        //Initializing the File toolbar
        initFileToolbar();
        
        //Initializing the Workspace
        initWorkspace();
        
        //Initialize the Event handlers on each button.
        initEventHandlers();
        
        primaryStage = primStage;
        initWindow(windowTitle);
    }
    
    private void initFileToolbar() {
        fileToolbarPane = new VBox();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_EPF_EDIT_VBOX);
        //Initializing the File toolbar buttons
        newEPfButton = initChildButton(fileToolbarPane, ICON_NEW_EPF, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "New ePortfolio" , false);
        loadEPfButton = initChildButton(fileToolbarPane, ICON_LOAD_EPF, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Load ePortfolio" , false);
        saveEPfButton = initChildButton(fileToolbarPane, ICON_SAVE_EPF, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Save" , true);
        saveAsEPfButton = initChildButton(fileToolbarPane, ICON_SAVE_AS_EPF, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Save As.." , true);
        exportButton = initChildButton(fileToolbarPane, ICON_EXPORT_EPF, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Export" , true);
        exitEPfButton = initChildButton(fileToolbarPane, ICON_EXIT, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Exit" , false);
    }
    
    private void initWorkspace() {
        ePfEditToolbar = new VBox();
        
        ePfEditToolbar.getStyleClass().add(CSS_CLASS_EPF_EDIT_VBOX);
        addPage = initChildButton(ePfEditToolbar, ICON_ADD_PAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Add Page" , true);
        slctLayout = initChildButton(ePfEditToolbar, ICON_SLCT_LAYOUT, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Select Layout", true);
        colorTemp = initChildButton(ePfEditToolbar, ICON_COLOR_TEMP, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Select Color Template", true);
        removePage = initChildButton(ePfEditToolbar, ICON_REMOVE_PAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Remove Page" , true);
        pageTitle = initChildButton(ePfEditToolbar, ICON_CHANGE_TITLE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Enter/Change Title" , true);
        siteViewer = initChildButton(ePfEditToolbar, ICON_SITE_VIEWER, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, "Show Site", true);
        
        
        slideShowToolbar = new HBox();
        slideShowToolbar.getStyleClass().add(CSS_FileToolbarPane);
        setBannerImg(initChildButton(slideShowToolbar, ICON_SLCT_BANNER, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Select Banner Image", true));
        addImg = initChildButton(slideShowToolbar, ICON_ADD_IMAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Add Image", true);
        addList = initChildButton(slideShowToolbar, ICON_ADD_LIST, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Add List", true);
        addParagraph = initChildButton(slideShowToolbar, ICON_ADD_PAR, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Add Paragraph", true);
        addHeader = initChildButton(slideShowToolbar, ICON_ADD_HEADER, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Add Header", true);
        footerTxt = initChildButton(slideShowToolbar, ICON_FOOTER_TEXT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Enter/Change text in footer" , true);
        studentName = initChildButton(slideShowToolbar, ICON_STUDENT_NAME, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Enter student name" , true);
        addSlideshow = initChildButton(slideShowToolbar, ICON_ADD_SLIDESHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Add Slide show", true);
        addVideo = initChildButton(slideShowToolbar, ICON_ADD_VIDEO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Add Video", true);
        addHyper = initChildButton(slideShowToolbar, ICON_ADD_HYPER, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Add Hyperlink", true);
        
        Separator separate = new Separator();
        separate.setOrientation(Orientation.VERTICAL);
        slideShowToolbar.getChildren().add(separate);
        
        setAddSlide(initChildButton(slideShowToolbar, ICON_ADD_SLIDE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Add slide", true));
        setMoveUp(initChildButton(slideShowToolbar, ICON_MOVE_UP, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Move up slide", true));
        setMoveDown(initChildButton(slideShowToolbar, ICON_MOVE_DOWN, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Move down slide", true));
        setRemoveSlide(initChildButton(slideShowToolbar, ICON_REMOVE_SLIDE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, "Remove slide", true));
        
        
    }
    
    public Button initChildButton(Pane toolbar, String iconFileName, String cssClass, String Tooltip, boolean disabled) {
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        javafx.scene.control.Button button = new javafx.scene.control.Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(Tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        getPrimaryStage().setTitle(windowTitle);
        
        
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        // AND USE IT TO SIZE THE WINDOW
        getPrimaryStage().setX(bounds.getMinX());
        getPrimaryStage().setY(bounds.getMinY());
        getPrimaryStage().setWidth(bounds.getWidth());
        getPrimaryStage().setHeight(bounds.getHeight());
        
        
        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        pageWorkspace = new TabPane();
        getPageWorkspace().getStyleClass().add(CSS_TAB_PANE);
        ePfPane = new BorderPane();
        ePfPane.getStyleClass().add(CSS_UI);
        ePfPane.setTop(slideShowToolbar);
        ePfPane.setLeft(fileToolbarPane);
        ePfPane.setRight(ePfEditToolbar);
        ePfPane.setCenter(getPageWorkspace());
        primaryScene = new Scene(ePfPane);
        
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        getPrimaryStage().setScene(primaryScene);
        getPrimaryStage().show();
    }
    
    private void initEventHandlers() {
        fileController = new FileController(this,  fileManager);
        
        newEPfButton.setOnAction(e -> {
            
            pageWorkspace.getTabs().clear();
            pages.clear();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
            alert.getDialogPane().getStyleClass().add(CSS_ALERT);
            alert.setTitle("New ePortfolio");
            alert.setHeaderText(null);
            alert.setContentText("Are you sure you want to create a new ePortfolio?");
            getAddSlide().setDisable(true);
            getRemoveSlide().setDisable(true);
            getMoveUp().setDisable(true);
            getMoveDown().setDisable(true);
            
            
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                Alert alertSave = new Alert(Alert.AlertType.CONFIRMATION);
                alertSave.setTitle("Save Option");
                alertSave.setHeaderText(null);
                alertSave.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                alertSave.getDialogPane().getStyleClass().add(CSS_ALERT);
                alertSave.setContentText("Would you like to save your work?");
                Optional<ButtonType> resultSave = alertSave.showAndWait();
                if(resultSave.get() == ButtonType.OK) {
                    try {
                        fileController.handleSaveEPf();
                    } catch (Exception ex) {
                        
                    }
                }
                primaryStage.close();
                startUI(primaryStage, "ePortfolio");
                addPage.setDisable(false);
                studentName.setDisable(false);
            }
            edited();
        });
        
        addImg.setOnAction(e -> {
            getAddSlide().setDisable(true);
            getRemoveSlide().setDisable(true);
            getMoveUp().setDisable(true);
            getMoveDown().setDisable(true);
            Stage newStage = new Stage();
            newStage.setTitle("Add Image");
            GridPane grid = new GridPane();
            Button browse = new Button("Browse");
            Scene scene = new Scene(grid, 450, 250);
            scene.getStylesheets().add(STYLE_SHEET_UI);
            grid.getStyleClass().add(CSS_ALERT);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));
            
            TextField width = new TextField();
            width.setPromptText("0");
            TextField height = new TextField();
            height.setPromptText("0");
            TextField caption = new TextField();
            caption.setPromptText("Enter caption!");
            TextField position = new TextField();
            position.setPromptText("Enter position!");
            
            
            grid.add(new Label("Width:"), 0, 0);
            grid.add(width, 2, 0);
            grid.add(new Label("Height:"), 0, 1);
            grid.add(height, 2, 1);
            grid.add(new Label("Caption:"), 0, 2);
            grid.add(caption, 2, 2);
            grid.add(new Label("Float:"), 0, 3);
            grid.add(position, 2, 3);
            grid.add(browse, 2, 4);
            
            newStage.setScene(scene);
            newStage.show();
            
            browse.setOnAction(f-> {
                FileChooser fileChooser = new FileChooser();
                FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
                FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
                fileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
                fileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
                File selectedFile = fileChooser.showOpenDialog(null);
                if(selectedFile != null && isNumber(width.getText()) && isNumber(height.getText())) {
                    ImageComp imgComp = new ImageComp();
                    imgComp.setWidth(Integer.valueOf(width.getText()));
                    imgComp.setHeight(Integer.valueOf(height.getText()));
                    imgComp.setCaption(caption.getText());
                    imgComp.setPosition(position.getText());
                    imgComp.setImgFileName(selectedFile.getName());
                    imgComp.setImgFilePath(selectedFile.getAbsolutePath());
                    selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                    for(int i = 0; i < getPageWorkspace().getTabs().size(); i++) {
                        Tab checkTab = getPageWorkspace().getTabs().get(i);
                        if(selectedTab.equals(checkTab)) {
                            PageModel thisPageModel = getPages().get(i);
                            thisPageModel.getComponents().add(imgComp);
                            reloadEPortfolio(thisPageModel);
                        }
                    }
                    newStage.close();
                    edited();
                }
            });
            
        });
        
        getBannerImg().setOnAction(e -> {
            getAddSlide().setDisable(true);
            getRemoveSlide().setDisable(true);
            getMoveUp().setDisable(true);
            getMoveDown().setDisable(true);
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
            FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
            FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
            fileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
            fileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
            File selectedFile = fileChooser.showOpenDialog(null);
            if(selectedFile != null) {
                BannerComp bannerComp = new BannerComp();
                bannerComp.setFileName(selectedFile.getName());
                bannerComp.setImgFilePath(selectedFile.getAbsolutePath());
                selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                for(int i = 0; i < getPageWorkspace().getTabs().size(); i++) {
                    Tab checkTab = getPageWorkspace().getTabs().get(i);
                    if(selectedTab.equals(checkTab)) {
                        PageModel thisPageModel = getPages().get(i);
                        thisPageModel.getComponents().add(bannerComp);
                        reloadEPortfolio(thisPageModel);
                    }
                }
                edited();
            }
        });
        
        saveAsEPfButton.setOnAction(e -> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                fileController.handleSaveEPf();
            } catch (Exception ex) {   
            }
            saveAsEPfButton.setDisable(true);
        });
        
        addVideo.setOnAction(e -> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                Stage newStage = new Stage();
                newStage.setTitle("Add Video");
                GridPane grid = new GridPane();
                Button browse = new Button("Browse");
                Scene scene = new Scene(grid, 450, 250);
                scene.getStylesheets().add(STYLE_SHEET_UI);
                grid.getStyleClass().add(CSS_ALERT);
                grid.setHgap(10);
                grid.setVgap(10);
                grid.setPadding(new Insets(20, 150, 10, 10));
                
                TextField width = new TextField();
                width.setPromptText("0");
                TextField height = new TextField();
                height.setPromptText("0");
                TextField caption = new TextField();
                caption.setPromptText("Enter caption!");
                TextField position = new TextField();
                position.setPromptText("Enter position!");
                
                
                grid.add(new Label("Width:"), 0, 0);
                grid.add(width, 2, 0);
                grid.add(new Label("Height:"), 0, 1);
                grid.add(height, 2, 1);
                grid.add(new Label("Caption:"), 0, 2);
                grid.add(caption, 2, 2);
                grid.add(new Label("Float:"), 0, 3);
                grid.add(position, 2, 3);
                grid.add(browse, 2, 4);
                
                newStage.setScene(scene);
                newStage.show();
                
                browse.setOnAction(f-> {
                    FileChooser fileChooser = new FileChooser();
                    FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
                    fileChooser.getExtensionFilters().addAll(mp4Filter);
                    File selectedFile = fileChooser.showOpenDialog(null);
                    if(selectedFile != null && isNumber(width.getText()) && isNumber(height.getText())) {
                        VideoComp videoComp = new VideoComp();
                        videoComp.setWidth(Integer.valueOf(width.getText()));
                        videoComp.setHeight(Integer.valueOf(height.getText()));
                        videoComp.setCaption(caption.getText());
                        videoComp.setPosition(position.getText());
                        videoComp.setVideoFileName(selectedFile.getName());
                        videoComp.setVideoFilePath(selectedFile.getAbsolutePath());
                        selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                        for(int i = 0; i < getPageWorkspace().getTabs().size(); i++) {
                            Tab checkTab = getPageWorkspace().getTabs().get(i);
                            if(selectedTab.equals(checkTab)) {
                                PageModel thisPageModel = getPages().get(i);
                                thisPageModel.getComponents().add(videoComp);
                                reloadEPortfolio(thisPageModel);
                            }
                        }
                        newStage.close();
                    }
                });
                edited();
            }catch(Exception ex) {
                
            }
        });
        
        addHyper.setOnAction(e -> {
            getAddSlide().setDisable(true);
            getRemoveSlide().setDisable(true);
            getMoveUp().setDisable(true);
            getMoveDown().setDisable(true);
            TextInputDialog ePfHyper = new TextInputDialog();
            ePfHyper.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
            ePfHyper.getDialogPane().getStyleClass().add(CSS_ALERT);
            ePfHyper.setTitle("Add Hyperlink");
            ePfHyper.setHeaderText(null);
            ePfHyper.setContentText("Please enter the Hyperlink.");
            
            
            Optional<String> answer = ePfHyper.showAndWait();
            edited();
        });
        
        getAddPage().setOnAction(e -> {
            pageModel = new PageModel(this, pageCounter);
            pageModel.setTitle("Page " + pageCounter);
            getPages().add(getPageModel());
            Tab newPage = new Tab();
            newPage.setId(String.valueOf(pageCounter));
            newPage.setText("New Page");
            newPage.setClosable(false);
            VBox vbox = new VBox();
            vbox.getStyleClass().add(CSS_PAGE_WORKSPACE);
            FlowPane Flowcontent = new FlowPane();
            Flowcontent.getChildren().add(vbox);
            newPage.setContent(Flowcontent);
            getPageWorkspace().getTabs().add(newPage);
            pageCounter++;
            colorTemp.setDisable(false);
            removePage.setDisable(false);
            slctLayout.setDisable(false);
            pageTitle.setDisable(false);
            siteViewer.setDisable(false);
            getBannerImg().setDisable(false);
            addImg.setDisable(false);
            addList.setDisable(false);
            addParagraph.setDisable(false);
            addHeader.setDisable(false);
            footerTxt.setDisable(false);
            addSlideshow.setDisable(false);
            addVideo.setDisable(false);
            addHyper.setDisable(false);
            getAddSlide().setDisable(true);
            getRemoveSlide().setDisable(true);
            getMoveUp().setDisable(true);
            getMoveDown().setDisable(true);
            edited();
        });
        
        exitEPfButton.setOnAction(e -> {
            
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
            alert.getDialogPane().getStyleClass().add(CSS_ALERT);
            alert.setTitle("Exit");
            alert.setHeaderText(null);
            alert.setContentText("Would you like to save before exiting?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                try {
                    fileController.handleSaveEPf();
                } catch (Exception ex) {
                    
                }
            }
            System.exit(0);
        });
        
        removePage.setOnAction(e -> {
            selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
            for(int j = 0; j < getPageWorkspace().getTabs().size(); j++) {
                Tab checkTab = getPageWorkspace().getTabs().get(j);
                if(selectedTab.equals(checkTab)) {
                    getPages().remove(j);
                    getPageWorkspace().getTabs().remove(j);
                }
            }
            if(getPageWorkspace().getTabs().size() == 0) {
                colorTemp.setDisable(true);
                removePage.setDisable(true);
                slctLayout.setDisable(true);
                pageTitle.setDisable(true);
                siteViewer.setDisable(true);
                getBannerImg().setDisable(true);
                addImg.setDisable(true);
                addList.setDisable(true);
                addParagraph.setDisable(true);
                addHeader.setDisable(true);
                footerTxt.setDisable(true);
                addSlideshow.setDisable(true);
                addVideo.setDisable(true);
                addHyper.setDisable(true);
            }
            edited();
        });
        
        
        pageTitle.setOnAction(e -> {
            selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
            TextInputDialog ePfTitle = new TextInputDialog(selectedTab.getText());
            ePfTitle.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
            ePfTitle.getDialogPane().getStyleClass().add(CSS_ALERT);
            ePfTitle.setTitle("Page Title");
            ePfTitle.setHeaderText(null);
            ePfTitle.setContentText("Please enter/change page Title");
            
            Optional<String> answer = ePfTitle.showAndWait();
            
            selectedTab.setText(answer.get());
            for(int i = 0; i < getPageWorkspace().getTabs().size(); i++) {
                Tab checkTab = getPageWorkspace().getTabs().get(i);
                if(selectedTab.equals(checkTab)) {
                    PageModel thisPageModel = getPages().get(i);
                    thisPageModel.setTitle(answer.get());
                }
            }
            edited();
        });
        
        colorTemp.setOnAction(e -> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                Stage newStage = new Stage();
                newStage.setTitle("ColorPicker");
                Scene scene = new Scene(new VBox(20), 400, 130);
                VBox box = (VBox) scene.getRoot();
                box.setPadding(new Insets(5, 5, 5, 5));
                Button okButton = new Button("Okay");
                
                final ColorPicker colorPicker = new ColorPicker();
                colorPicker.setValue(Color.CORAL);
                
                final Text text = new Text("Looks Good!!");
                text.setFont(Font.font ("Verdana", 20));
                text.setFill(colorPicker.getValue());
                
                colorPicker.setOnAction((ActionEvent f) -> {
                    text.setFill(colorPicker.getValue());
                });
                
                box.getChildren().addAll(colorPicker, text, okButton);
                
                newStage.setScene(scene);
                newStage.show();
                
                okButton.setOnAction(f -> {
                    ColorComp colorComp = new ColorComp(colorPicker.getValue());
                    colorComp.setColor(colorPicker.getValue().toString());
                    selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                    for(int j = 0; j < getPageWorkspace().getTabs().size(); j++) {
                        Tab checkTab = getPageWorkspace().getTabs().get(j);
                        if(selectedTab.equals(checkTab)) {
                            PageModel thisPageModel = getPages().get(j);
                            thisPageModel.getComponents().add(colorComp);
                            thisPageModel.setColor(colorPicker.getValue().toString());
                            reloadEPortfolio(thisPageModel);
                        }
                    }
                    newStage.close();
                    colorTemp.setDisable(true);
                    
                });
                edited();
            }catch(Exception ex) {
            }
        });
        
        addList.setOnAction(e -> {
            try {
                TextInputDialog ePfLists = new TextInputDialog();
                ePfLists.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                ePfLists.getDialogPane().getStyleClass().add(CSS_ALERT);
                ePfLists.setTitle("List Option");
                ePfLists.setHeaderText(null);
                ePfLists.setContentText("Please enter number of list items.");
                
                Optional<String> answer = ePfLists.showAndWait();
                Stage newStage = new Stage();
                Scene scene = new Scene(new VBox(20), 400, 450);
                VBox box = (VBox) scene.getRoot();
                scene.getStylesheets().add(STYLE_SHEET_UI);
                box.getStyleClass().add(CSS_ALERT);
                box.setPadding(new Insets(5, 5, 5, 5));
                Button okay = new Button("Okay");
                
                for(int i=0; i< Integer.valueOf(answer.get()); i++) {
                    newStage.setTitle("Add List Items");
                    Label listItemLabel = new Label("List " + (i + 1));
                    TextField listItem = new TextField();
                    listItem.setPromptText("Enter your text here!");
                    HBox listItemBox = new HBox(15);
                    listItemBox.getChildren().add(listItemLabel);
                    listItemBox.getChildren().add(listItem);
                    box.getChildren().add(listItemBox);
                }
                
                ListComp listComp = new ListComp();
                okay.setOnAction(f -> {
                    for(int i = 0; i < box.getChildren().size() - 1; i++) {
                        HBox hboxx = (HBox) box.getChildren().get(i);
                        TextField textt = (TextField) hboxx.getChildren().get(1);
                        listComp.getListArray().add(textt.getText());
                    }
                    
                    selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                    for(int j = 0; j < getPageWorkspace().getTabs().size(); j++) {
                        Tab checkTab = getPageWorkspace().getTabs().get(j);
                        if(selectedTab.equals(checkTab)) {
                            PageModel thisPageModel = getPages().get(j);
                            thisPageModel.getComponents().add(listComp);
                            reloadEPortfolio(thisPageModel);
                        }
                    }
                    
                    newStage.close();
                });
                
                box.getChildren().add(okay);
                newStage.setScene(scene);
                newStage.show();
                edited();
            } catch(Exception x) {
                
            }
        });
        
        addParagraph.setOnAction(e -> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                Stage newStage = new Stage();
                newStage.setTitle("Add Paragraph");
                Scene scene = new Scene(new VBox(20), 400, 270);
                VBox box = (VBox) scene.getRoot();
                scene.getStylesheets().add(STYLE_SHEET_UI);
                box.getStyleClass().add(CSS_ALERT);
                box.setPadding(new Insets(5, 5, 5, 5));
                ComboBox comboBox = new ComboBox();
                comboBox.getItems().addAll("Times New Roman", "Arial", "Brush Script MT");
                comboBox.setValue(comboBox.getItems().get(0));
                TextArea paragraph = new TextArea();
                TextField fontSize = new TextField();
                paragraph.setMaxWidth(Double.MAX_VALUE);
                paragraph.setMaxHeight(Double.MAX_VALUE);
                fontSize.setMaxHeight(Double.MAX_VALUE);
                paragraph.setWrapText(true);
                paragraph.setPromptText("You can start typing!");
                fontSize.setPromptText("Enter Font Size");
                Button okButton = new Button("Okay");
                
                
                okButton.setOnAction(f -> {
                    if(isNumber(fontSize.getText())) {
                        ParagraphComp parComp = new ParagraphComp();
                        parComp.setText(paragraph.getText());
                        parComp.setFont((String)comboBox.getValue());
                        parComp.setFontSize(fontSize.getText());
                        selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                        for(int i = 0; i < getPageWorkspace().getTabs().size(); i++) {
                            Tab checkTab = getPageWorkspace().getTabs().get(i);
                            if(selectedTab.equals(checkTab)) {
                                PageModel thisPageModel = getPages().get(i);
                                thisPageModel.getComponents().add(parComp);
                                reloadEPortfolio(thisPageModel);
                            }
                        }
                        newStage.close();
                    }
                });
                box.getChildren().addAll(comboBox, paragraph, fontSize, okButton);
                newStage.setScene(scene);
                newStage.show();
                edited();
            } catch(Exception x) {
                
            }
        });
        
        addHeader.setOnAction(e -> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                Stage newStage = new Stage();
                newStage.setTitle("Add Header");
                Scene scene = new Scene(new VBox(20), 400, 270);
                VBox box = (VBox) scene.getRoot();
                scene.getStylesheets().add(STYLE_SHEET_UI);
                box.getStyleClass().add(CSS_ALERT);
                box.setPadding(new Insets(5, 5, 5, 5));
                ComboBox comboBox = new ComboBox();
                comboBox.getItems().addAll("Times New Roman", "Arial", "Brush Script MT");
                comboBox.setValue(comboBox.getItems().get(0));
                TextArea header = new TextArea();
                TextField fontSize = new TextField();
                header.setMaxWidth(Double.MAX_VALUE);
                header.setMaxHeight(Double.MAX_VALUE);
                fontSize.setMaxHeight(Double.MAX_VALUE);
                header.setWrapText(true);
                header.setPromptText("You can start typing!");
                fontSize.setPromptText("Enter Font Size!");
                Button okay = new Button("Okay");
                
                okay.setOnAction(f -> {
                    if(isNumber(fontSize.getText())) {
                        HeaderComp headerComp = new HeaderComp();
                        headerComp.setText(header.getText());
                        headerComp.setFont((String)comboBox.getValue());
                        headerComp.setFontSize(fontSize.getText());
                        selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                        for(int i = 0; i < getPageWorkspace().getTabs().size(); i++) {
                            Tab checkTab = getPageWorkspace().getTabs().get(i);
                            if(selectedTab.equals(checkTab)) {
                                PageModel thisPageModel = getPages().get(i);
                                thisPageModel.getComponents().add(headerComp);
                                reloadEPortfolio(thisPageModel);
                            }
                        }
                        newStage.close();
                    }
                });
                
                box.getChildren().addAll(comboBox, header, fontSize, okay);
                newStage.setScene(scene);
                newStage.show();
                edited();
            } catch(Exception a) {
                
            }
        });
        
        footerTxt.setOnAction(e -> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                Stage newStage = new Stage();
                newStage.setTitle("Add Footer");
                Scene scene = new Scene(new VBox(20), 400, 270);
                VBox box = (VBox) scene.getRoot();
                scene.getStylesheets().add(STYLE_SHEET_UI);
                box.getStyleClass().add(CSS_ALERT);
                box.setPadding(new Insets(5, 5, 5, 5));
                ComboBox comboBox = new ComboBox();
                comboBox.getItems().addAll("Times New Roman", "Arial", "Brush Script MT");
                comboBox.setValue(comboBox.getItems().get(0));
                TextArea footer = new TextArea();
                TextField fontSize = new TextField();
                footer.setMaxWidth(Double.MAX_VALUE);
                footer.setMaxHeight(Double.MAX_VALUE);
                fontSize.setMaxHeight(Double.MAX_VALUE);
                footer.setWrapText(true);
                footer.setPromptText("You can start typing!");
                fontSize.setPromptText("Enter Font Size!");
                Button okay = new Button("Okay");
                
                okay.setOnAction(f -> {
                    if(isNumber(fontSize.getText())) {
                        FooterComp footerComp = new FooterComp();
                        footerComp.setText(footer.getText());
                        footerComp.setFont((String)comboBox.getValue());
                        footerComp.setFontSize(fontSize.getText());
                        selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                        for(int i = 0; i < getPageWorkspace().getTabs().size(); i++) {
                            Tab checkTab = getPageWorkspace().getTabs().get(i);
                            if(selectedTab.equals(checkTab)) {
                                PageModel thisPageModel = getPages().get(i);
                                thisPageModel.getComponents().add(footerComp);
                                reloadEPortfolio(thisPageModel);
                            }
                        }
                        newStage.close();
                    }
                });
                
                box.getChildren().addAll(comboBox, footer, fontSize, okay);
                
                newStage.setScene(scene);
                newStage.show();
                edited();
            }catch(Exception ex) {
                
            }
        });
        
        getStudentName().setOnAction(e -> {
            try {
                TextInputDialog ePfStudentName = new TextInputDialog(getPrimaryStage().getTitle());
                ePfStudentName.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                ePfStudentName.getDialogPane().getStyleClass().add(CSS_ALERT);
                ePfStudentName.setTitle("First Last");
                ePfStudentName.setHeaderText(null);
                ePfStudentName.setContentText("Please enter your First Last name.");
                
                Optional<String> studentName = ePfStudentName.showAndWait();
                if(studentName.get().length() > 0) {
                    getPrimaryStage().setTitle(studentName.get());
                }
                edited();
            }catch(Exception ex) {
                
            }
        });
        
        addSlideshow.setOnAction(e->{
            try {
                TextInputDialog ePfSlideshow = new TextInputDialog();
                ePfSlideshow.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                ePfSlideshow.getDialogPane().getStyleClass().add(CSS_ALERT);
                ePfSlideshow.setTitle("Slideshow Option");
                ePfSlideshow.setHeaderText(null);
                ePfSlideshow.setContentText("Please enter number of slides.");
                
                Optional<String> answer = ePfSlideshow.showAndWait();
                ArrayList<String> arrayPath = new ArrayList<String>();
                ArrayList<String> arrayCaption = new ArrayList<String>();
                for(int i=0; i< Integer.valueOf(answer.get()); i++) {
                    String imagePath = "file:" + PATH_SLIDE_SHOW_IMAGES + DEFAULT_SLIDE_IMAGE;
                    TextField caption = new TextField();
                    caption.setPromptText("Empty");
                    arrayPath.add(imagePath);
                    arrayCaption.add(caption.getText());
                }
                
                SlideshowComp slideshowComp = new SlideshowComp();
                for(int i = 0; i < arrayPath.size(); i++) {
                    slideshowComp.getImages().add(arrayPath.get(i));
                    slideshowComp.getCaptions().add(arrayCaption.get(i));
                }
                
                selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                for(int j = 0; j < getPageWorkspace().getTabs().size(); j++) {
                    Tab checkTab = getPageWorkspace().getTabs().get(j);
                    if(selectedTab.equals(checkTab)) {
                        PageModel thisPageModel = getPages().get(j);
                        thisPageModel.getComponents().add(slideshowComp);
                        reloadEPortfolio(thisPageModel);
                    }
                }
                edited();
            }catch(Exception ex) {
                
            }
        });
        
        saveEPfButton.setOnAction(e -> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                fileController.handleSaveEPf();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            saveEPfButton.setDisable(true);
            
        });
        
        loadEPfButton.setOnAction(e -> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.getDialogPane().getStylesheets().add(STYLE_SHEET_UI);
                alert.getDialogPane().getStyleClass().add(CSS_ALERT);
                alert.setTitle("Loading");
                alert.setHeaderText(null);
                alert.setContentText("Would you like to save before loading?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    try {
                        fileController.handleSaveEPf();
                    } catch (Exception ex) {
                        
                    }
                }
                fileController.handleLoadEPf();
            } catch (Exception ex) {
                
            }
        });
        
        siteViewer.setOnAction(e-> {
            try {
                getAddSlide().setDisable(true);
                getRemoveSlide().setDisable(true);
                getMoveUp().setDisable(true);
                getMoveDown().setDisable(true);
                fileController.handleWebViewRequest();
            } catch (Exception ex) {
                
            }
        });
        
        slctLayout.setOnAction(e -> {
            getAddSlide().setDisable(true);
            getRemoveSlide().setDisable(true);
            getMoveUp().setDisable(true);
            getMoveDown().setDisable(true);
            Stage newStage = new Stage();
            newStage.setTitle("Select Layout");
            Scene scene = new Scene(new VBox(20), 400, 270);
            VBox box = (VBox) scene.getRoot();
            scene.getStylesheets().add(STYLE_SHEET_UI);
            box.getStyleClass().add(CSS_ALERT);
            box.setPadding(new Insets(5, 5, 5, 5));
            ComboBox comboBox = new ComboBox();
            comboBox.getItems().addAll("Select Layout","Layout 1", "Layout 2", "Layout 3", "Layout 4", "Layout 5", "Layout 6");
            comboBox.setValue(comboBox.getItems().get(0));
            Button okButton = new Button("Okay");
            box.getChildren().add(comboBox);
            
            comboBox.setOnAction(d -> {
                if(comboBox.getValue().toString().equals("Layout 1")) {
                    box.getChildren().clear();
                    box.getChildren().add(comboBox);
                    final Text text1 = new Text("Banner Image available!");
                    final Text text2 = new Text("Nav bar centered under Banner!");
                    text1.setFill(Color.WHITE);
                    text2.setFill(Color.WHITE);
                    text1.setFont(Font.font ("Verdana", 12));
                    text2.setFont(Font.font ("Verdana", 12));
                    box.getChildren().addAll(text1, text2, okButton);
                }
                else if(comboBox.getValue().toString().equals("Layout 2")) {
                    box.getChildren().clear();
                    box.getChildren().add(comboBox);
                    final Text text1 = new Text("Nav bar on top!");
                    final Text text2 = new Text("Banner Image under Nav bar!");
                    text1.setFill(Color.WHITE);
                    text2.setFill(Color.WHITE);
                    text1.setFont(Font.font ("Verdana", 12));
                    text2.setFont(Font.font ("Verdana", 12));
                    box.getChildren().addAll(text1, text2, okButton);
                }
                else if(comboBox.getValue().toString().equals("Layout 3")) {
                    box.getChildren().clear();
                    box.getChildren().add(comboBox);
                    final Text text1 = new Text("Nav bar on left!");
                    final Text text2 = new Text("No Banner Image!");
                    text1.setFill(Color.WHITE);
                    text2.setFill(Color.WHITE);
                    text1.setFont(Font.font ("Verdana", 12));
                    text2.setFont(Font.font ("Verdana", 12));
                    box.getChildren().addAll(text1, text2, okButton);
                }
                else if(comboBox.getValue().toString().equals("Layout 4")) {
                    box.getChildren().clear();
                    box.getChildren().add(comboBox);
                    final Text text1 = new Text("Nav bar centered on top!");
                    final Text text2 = new Text("Banner Image under Nav bar!");
                    text1.setFill(Color.WHITE);
                    text2.setFill(Color.WHITE);
                    text1.setFont(Font.font ("Verdana", 12));
                    text2.setFont(Font.font ("Verdana", 12));
                    box.getChildren().addAll(text1, text2, okButton);
                }
                else if(comboBox.getValue().toString().equals("Layout 5")) {
                    box.getChildren().clear();
                    box.getChildren().add(comboBox);
                    final Text text1 = new Text("Nav bar on top!");
                    final Text text2 = new Text("No Banner Image!");
                    text1.setFill(Color.WHITE);
                    text2.setFill(Color.WHITE);
                    text1.setFont(Font.font ("Verdana", 12));
                    text2.setFont(Font.font ("Verdana", 12));
                    box.getChildren().addAll(text1, text2, okButton);
                }
                else if(comboBox.getValue().toString().equals("Layout 6")) {
                    box.getChildren().clear();
                    box.getChildren().add(comboBox);
                    final Text text1 = new Text("Banner Image available!");
                    final Text text2 = new Text("Nav bar on left!");
                    text1.setFill(Color.WHITE);
                    text2.setFill(Color.WHITE);
                    text1.setFont(Font.font ("Verdana", 12));
                    text2.setFont(Font.font ("Verdana", 12));
                    box.getChildren().addAll(text1, text2, okButton);
                }
                else if(comboBox.getValue().toString().equals("Select Layout")) {
                    box.getChildren().clear();
                    box.getChildren().add(comboBox);
                }
            });
            newStage.setScene(scene);
            newStage.show();
            
            
            okButton.setOnAction(f -> {
                LayoutComp layoutComp = new LayoutComp();
                layoutComp.setLayout(comboBox.getValue().toString());
                selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
                for(int i = 0; i < getPageWorkspace().getTabs().size(); i++) {
                    Tab checkTab = getPageWorkspace().getTabs().get(i);
                    if(selectedTab.equals(checkTab)) {
                        PageModel thisPageModel = getPages().get(i);
                        thisPageModel.getComponents().add(layoutComp);
                        reloadEPortfolio(thisPageModel);
                    }
                }
                newStage.close();
            });
            newStage.setScene(scene);
            newStage.show();
            edited();
            slctLayout.setDisable(true);
        });
        
    }
    
    public void reloadEPortfolio(PageModel pageToLoad) {
        selectedTab = getPageWorkspace().getSelectionModel().getSelectedItem();
        selectedPane = (FlowPane) selectedTab.getContent();
        selectedBox = (VBox) selectedPane.getChildren().get(0);
        selectedBox.getChildren().clear();
        for (Component comp : pageToLoad.getComponents()) {
            ComponentEditView compEditor = new ComponentEditView(comp, pageToLoad);
            selectedBox.getChildren().add(compEditor);
        }
    }
    
    public void loadEPortfolio(PageModel pageToLoad, int i) {
        selectedTab = getPageWorkspace().getTabs().get(i);
        selectedPane = (FlowPane) selectedTab.getContent();
        selectedBox = (VBox) selectedPane.getChildren().get(0);
        selectedBox.getChildren().clear();
        for (Component comp : pageToLoad.getComponents()) {
            ComponentEditView compEditor = new ComponentEditView(comp, pageToLoad);
            selectedBox.getChildren().add(compEditor);
        }
    }
    
    
    
    public void edited() {
        saveEPfButton.setDisable(false);
        saveAsEPfButton.setDisable(false);
    }
    
    public void updateButtons() {
        addPage.setDisable(false);
        colorTemp.setDisable(false);
        removePage.setDisable(false);
        slctLayout.setDisable(false);
        pageTitle.setDisable(false);
        siteViewer.setDisable(false);
        getBannerImg().setDisable(false);
        addImg.setDisable(false);
        addList.setDisable(false);
        addParagraph.setDisable(false);
        addHeader.setDisable(false);
        footerTxt.setDisable(false);
        addSlideshow.setDisable(false);
        addVideo.setDisable(false);
        addHyper.setDisable(false);
    }
    
    public boolean isNumber(String x) {
        try{
            Integer.parseInt(x);
            return true;
        }catch(Exception e) {
            return false;
        }
    }
    
    /**
     * @return the moveUp
     */
    public Button getMoveUp() {
        return moveUp;
    }
    
    /**
     * @param moveUp the moveUp to set
     */
    public void setMoveUp(Button moveUp) {
        this.moveUp = moveUp;
    }
    
    /**
     * @return the moveDown
     */
    public Button getMoveDown() {
        return moveDown;
    }
    
    /**
     * @param moveDown the moveDown to set
     */
    public void setMoveDown(Button moveDown) {
        this.moveDown = moveDown;
    }
    
    /**
     * @return the addSlide
     */
    public Button getAddSlide() {
        return addSlide;
    }
    
    /**
     * @param addSlide the addSlide to set
     */
    public void setAddSlide(Button addSlide) {
        this.addSlide = addSlide;
    }
    
    /**
     * @return the removeSlide
     */
    public Button getRemoveSlide() {
        return removeSlide;
    }
    
    /**
     * @param removeSlide the removeSlide to set
     */
    public void setRemoveSlide(Button removeSlide) {
        this.removeSlide = removeSlide;
    }
    
    /**
     * @return the newEPfButton
     */
    public Button getNewEPfButton() {
        return newEPfButton;
    }
    
    /**
     * @return the addPage
     */
    public Button getAddPage() {
        return addPage;
    }
    
    /**
     * @return the primaryStage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }
    
    /**
     * @return the saveEPfButton
     */
    public Button getSaveEPfButton() {
        return saveEPfButton;
    }
    
    /**
     * @return the saveAsEPfButton
     */
    public Button getSaveAsEPfButton() {
        return saveAsEPfButton;
    }
    
    /**
     * @return the pageModel
     */
    public PageModel getPageModel() {
        return pageModel;
    }
    
    /**
     * @return the pages
     */
    public ObservableList<PageModel> getPages() {
        return pages;
    }
    
    /**
     * @return the studentName
     */
    public Button getStudentName() {
        return studentName;
    }
    
    /**
     * @return the pageWorkspace
     */
    public TabPane getPageWorkspace() {
        return pageWorkspace;
    }
    
    /**
     * @return the selectBannerImg
     */
    public Button getSelectBannerImg() {
        return selectBannerImg;
    }
    
    /**
     * @param selectBannerImg the selectBannerImg to set
     */
    public void setSelectBannerImg(Button selectBannerImg) {
        this.selectBannerImg = selectBannerImg;
    }
    
    /**
     * @return the bannerImg
     */
    public Button getBannerImg() {
        return bannerImg;
    }
    
    /**
     * @param bannerImg the bannerImg to set
     */
    public void setBannerImg(Button bannerImg) {
        this.bannerImg = bannerImg;
    }
}
