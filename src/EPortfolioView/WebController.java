/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioView;


import static EPortfolioView.StartupConstants.CSS_UI;
import java.net.URL;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import java.nio.file.Paths;
import java.nio.file.Path;
/**
 *
 * @author KhaledAshraf
 */
public class WebController extends Region {
    final WebView web = new WebView();
    final WebEngine engine = web.getEngine();
    
    public WebController(String indexPath) {
        //apply the styles
        getStyleClass().add(CSS_UI);
        Path path = Paths.get("data/index/public_html/index.html");
        try {
            URL url = path.toUri().toURL();
            engine.load(url.toExternalForm());
            getChildren().add(web);
        }catch(Exception e) {
            
        }
    }
}


