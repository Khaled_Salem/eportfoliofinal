/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolioView;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author KhaledAshraf
 */
public class ImageCurrentView {
    private ImageView currentImageView;
    private int imageNumber;
    
    public ImageCurrentView(Image currentImage, int imageNumber) {
        this.currentImageView = new ImageView(currentImage);
        this.imageNumber = imageNumber;
    }

    /**
     * @return the currentImageView
     */
    public ImageView getCurrentImageView() {
        return currentImageView;
    }

    /**
     * @param currentImageView the currentImageView to set
     */
    public void setCurrentImageView(ImageView currentImageView) {
        this.currentImageView = currentImageView;
    }

    /**
     * @return the imageNumber
     */
    public int getImageNumber() {
        return imageNumber;
    }

    /**
     * @param imageNumber the imageNumber to set
     */
    public void setImageNumber(int imageNumber) {
        this.imageNumber = imageNumber;
    }
}
