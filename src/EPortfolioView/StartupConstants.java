package EPortfolioView;
/**
 *
 * @author KhaledAshraf
 */
public class StartupConstants {

    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String UI_PROPERTIES_ENG_FILE_NAME = "properties_EN.xml";
    public static String UI_PROPERTIES_ES_FILE_NAME = "properties_ES.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_SLIDE_SHOWS = PATH_DATA + "slide_shows/";
    public static String PATH_EPFS = PATH_DATA + "ePfs/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_CSS = "style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolioStyle.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_NEW_EPF = "NewEpf.png";
    public static String ICON_LOAD_EPF = "Load.png";
    public static String ICON_SAVE_EPF = "Save.png";
    public static String ICON_SAVE_AS_EPF = "saveAs.png";
    public static String ICON_EXPORT_EPF = "export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_PAGE = "AddPage.png";
    public static String ICON_ADD_SLIDE = "Add.png";
    public static String ICON_REMOVE_PAGE = "RemovePage.png";
    public static String ICON_REMOVE_SLIDE = "removeSlide.png";
    public static String ICON_MOVE_UP = "moveUp.png";
    public static String ICON_MOVE_DOWN = "moveDown.png";
    public static String ICON_CHANGE_TITLE = "edit.png";
    public static String ICON_STUDENT_NAME = "student.png";
    public static String ICON_FOOTER_TEXT = "footer.png";
    public static String ICON_SLCT_LAYOUT = "slctLayout.png";
    public static String ICON_COLOR_TEMP = "pageColor.png";
    public static String ICON_PAGE_FONT = "pageFont.png";
    public static String ICON_SITE_VIEWER = "website.png";
    public static String ICON_SLCT_BANNER = "bannerImg.png";
    public static String ICON_ADD_IMAGE = "addImage.png";
    public static String ICON_ADD_LIST = "addList.png";
    public static String ICON_ADD_PAR = "addPar.png";
    public static String ICON_ADD_HEADER = "addHeader.png";
    public static String ICON_ADD_SLIDESHOW = "addSlideshow.png";
    public static String ICON_ADD_VIDEO = "addVideo.png";
    public static String ICON_ADD_HYPER = "addHyper.png";

    // @todo
    public static String    DEFAULT_SLIDE_IMAGE = "DefaultStartSlide.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 250;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 600;
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_EPF_EDIT_VBOX = "epf_edit_vbox";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SLIDE_HIGHLIGHT = "border_style";
    public static String    CSS_UI  = "workspace";
    public static String    CSS_VBox = "slide_show_vbox";
    public static String    CSS_Scroll = "slide_show_scrollpane";
    public static String    CSS_FileToolbarPane = "slide_show_filetoolbar";
    public static String    CAPTION_TextField = "caption_TextField";
    public static String    CSS_ALERT = "alert";
    public static String    CSS_TAB_PANE = "tabPane";
    public static String    CSS_PAGE_WORKSPACE = "page_workspace";
    public static String    CAPTION_VBOX = "slide_vbox";
    // UI LABELS
    public static String    LABEL_SLIDE_SHOW_TITLE = "slide_show_title";
}

