package eportfolio;
import EPortfolioView.EPortfolioView;
import static EPortfolioView.StartupConstants.CSS_ALERT;
import static EPortfolioView.StartupConstants.STYLE_SHEET_UI;
import FileManager.EPortfolioFileManager;
import java.util.Optional;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

/**
 *
 * @author KhaledAshraf
 */
public class EPortfolio extends Application {
    EPortfolioFileManager fileManager = new EPortfolioFileManager();
    EPortfolioView ui = new EPortfolioView(fileManager);
    
    public void start(Stage primaryStage) throws Exception {
        
        ui.startUI(primaryStage, "ePortfolio");
        
        ui.getNewEPfButton().setOnAction(e -> {
                ui.getPrimaryStage().close();
                ui.startUI(primaryStage, "ePortfolio");
                ui.getAddPage().setDisable(false);
                ui.getSaveEPfButton().setDisable(false);
                ui.getSaveAsEPfButton().setDisable(false);
                ui.getStudentName().setDisable(false);
                ui.getPageWorkspace().getTabs().clear();
                ui.getPages().clear();
        });
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
