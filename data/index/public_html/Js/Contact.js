$(document).ready(function() {
    $.getJSON("Js/SlideShow.json", function(data) {
        document.getElementById("profile_pic_contact").src = "Imgs/" + data.profile.image_file_name;
        document.getElementById("home").innerHTML = data.home;
        document.getElementById("video").innerHTML = data.video_button;
        document.getElementById("projects").innerHTML = data.projects;
        document.getElementById("contact").innerHTML = data.contact;
        document.getElementById("bannerimg").src = "Icons/" + data.sbu_banner.image_file_name;
        document.getElementById("list1").innerHTML = data.list1;
        document.getElementById("list2").innerHTML = data.list2;
        document.getElementById("list3").innerHTML = data.list3;
        document.getElementById("list4").innerHTML = data.list4;

    })
})
