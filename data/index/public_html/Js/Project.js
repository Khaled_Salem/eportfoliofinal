$(document).ready(function() {
    $.getJSON("Js/SlideShow.json", function(data) {
        document.getElementById("profile_pic_contact").src = "Imgs/" + data.profile.image_file_name;
        document.getElementById("home").innerHTML = data.home;
        document.getElementById("video").innerHTML = data.video_button;
        document.getElementById("projects").innerHTML = data.projects;
        document.getElementById("contact").innerHTML = data.contact;
        document.getElementById("bannerimg").src = "Icons/" + data.sbu_banner.image_file_name;
        document.getElementById("banner_par").innerHTML = data.name;
        document.getElementById("img1").src = "Imgs/" + data.screenshot.image_file_name;
        document.getElementById("desc1").innerHTML = data.desc1;
        document.getElementById("par1").innerHTML = data.par1;
        document.getElementById("img2").src = "Imgs/" + data.screenshot2.image_file_name;
        document.getElementById("desc2").innerHTML = data.desc2;
        document.getElementById("par2").innerHTML = data.par2;
        document.getElementById("img3").src = "Imgs/" + data.screenshot3.image_file_name;
        document.getElementById("desc3").innerHTML = data.desc3;
        document.getElementById("par3").innerHTML = data.par3;
        document.getElementById("img4").src = "Imgs/" + data.screenshot4.image_file_name;
        document.getElementById("desc4").innerHTML = data.desc4;
        document.getElementById("par4").innerHTML = data.par4;
    })
})
