$(document).ready(function() {
    $.getJSON("Js/SlideShow.json", function(data) {
        document.getElementById("profile_pic_video").src = "Imgs/" + data.profile.image_file_name;
        document.getElementById("movie").src = "Imgs/" + data.video.image_file_name;
        document.getElementById("home").innerHTML = data.home;
        document.getElementById("video").innerHTML = data.video_button;
        document.getElementById("projects").innerHTML = data.projects;
        document.getElementById("contact").innerHTML = data.contact;
        document.getElementById("bannerimg").src = "Icons/" + data.sbu_banner.image_file_name;
    })
})
