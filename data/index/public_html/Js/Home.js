var i = 0;
var slides = new Array();
var sleep;


$(document).ready(function() {
    $.getJSON("Js/SlideShow.json", function(data) {
        slides = data.slides;
        document.getElementById("slideshow").src = "Imgs/" + slides[0].image_file_name;
        document.getElementById("caption").innerHTML = slides[0].caption;
        document.getElementById("profile_pic").src = "Imgs/" + data.profile.image_file_name;
        document.getElementById("profile_name").innerHTML = data.name;
        document.getElementById("bio").innerHTML = data.bio;
        document.getElementById("home").innerHTML = data.home;
        document.getElementById("video").innerHTML = data.video_button;
        document.getElementById("projects").innerHTML = data.projects;
        document.getElementById("contact").innerHTML = data.contact;
        document.getElementById("bannerimg_home").src = "Icons/" + data.bannerimg_home.image_file_name;
        document.getElementById("prev").src = "Icons/" + data.prev.image_file_name;
        document.getElementById("pause").src = "Icons/" + data.pause.image_file_name;
        document.getElementById("next").src = "Icons/" + data.next.image_file_name;
    })
})

function PlaySlides() {
    if(choice == "play") {
        sleep = 0;           
        document.getElementById("pause").hidden="";
        document.getElementById("prev").disabled = true;
        document.getElementById("next").disabled = true;
        document.getElementById("slideshow").src = "Imgs/" + slides[i].image_file_name;
        document.getElementById("caption").innerHTML = slides[i].caption;

        if (i < slides.length - 1) 
            i++;
        else
            i = 0;

        sleep = setTimeout("PlaySlides()", 3000); 

    }
    else if(choice == "pause") {
        document.getElementById("pause").hidden="hidden";
        document.getElementById("prev").hidden="";
        document.getElementById("next").hidden="";
        sleep = 0;
    }
    else if(choice == "next") {
        document.getElementById("pause").hidden="hidden";
        document.getElementById("prev").hidden="";
        document.getElementById("next").hidden="";
        if(i < slides.length - 1)
            i++;
        else
        i = 0;
        document.getElementById("slideshow").src = "Imgs/" + slides[i].image_file_name;
        document.getElementById("caption").innerHTML = slides[i].caption;
    }
    else if(choice == "prev") {
        document.getElementById("pause").hidden="hidden";
        document.getElementById("prev").hidden="";
        document.getElementById("next").hidden="";
        if(i > 0)
            i--;
        else
            i = slides.length - 1;
        document.getElementById("slideshow").src = "Imgs/" + slides[i].image_file_name;
        document.getElementById("caption").innerHTML = slides[i].caption;
    }
}

    function whichButton(which) {
        choice = which;
    }